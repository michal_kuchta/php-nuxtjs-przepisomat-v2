import Notifications from 'vue-notification';
import Vue from 'vue';

Vue.use(Notifications);

export default (context, inject) => {
    inject('notify', Vue.notify)
}