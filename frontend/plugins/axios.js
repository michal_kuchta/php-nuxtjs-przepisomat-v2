export default function ({app, $axios, store, $notify}) {
    $axios.onError(error => {
        if (error.response.status === 401 && error.response.headers['access-token']) {
            let config = error.config;
            app.$auth.setToken('local', error.response.headers['access-token']);
            $axios.setToken(error.response.headers['access-token'], 'Bearer');
            config.headers['Authorization'] = "Bearer " + error.response.headers['access-token'];

            return $axios.request(config);
        }

        if (error.response.status === 422) {
            store.dispatch('validation/setErrors', error.response.data.errors);
        } else {
            let type = error.response.data.type ? error.response.data.type : 'error';

            if (error.response && error.response.data && error.response.data.error) //Single error
                $notify({type: type, text: error.response.data.error});

            if (error.response && error.response.data && error.response.data.errors && error.response.data.errors.length > 0) //Multiple errors
            {
                error.response.data.errors.forEach(errorMessage => {
                    $notify({type: type, text: errorMessage});
                });
            }
        }

        return Promise.reject(error);
    });

    $axios.onRequest(() => {
        store.dispatch('validation/clearErrors');
    })
}
