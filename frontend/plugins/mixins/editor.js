import Vue from 'vue';

import {mapGetters} from 'vuex';

const Editor = {
    install(Vue, options) {
        Vue.mixin({
            computed: {
                ...mapGetters({
                    editorConfig: 'editor/editorConfig',
                    editorApiKey: 'editor/editorApiKey',
                })
            }
        });
    }
}

Vue.use(Editor);
