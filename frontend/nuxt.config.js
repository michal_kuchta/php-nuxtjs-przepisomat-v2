module.exports = {
    /*
    ** Headers of the page
    */
    head: {
        title: 'Przepisomat',
        meta: [
            {charset: 'utf-8'},
            {name: 'viewport', content: 'width=device-width, initial-scale=1'},
            {hid: 'description', name: 'description', content: 'Przepisomat'}
        ],
        link: [
            {rel: 'icon', type: 'image/x-icon', href: '/favicon.ico'}
        ],
    },
    /*
    ** Customize the progress bar color
    */
    loading: {
        color: '#3B8070',
        height: '5px'
    },
    loadingIndicator: {
        name: 'chasing-dots',
        color: '#3B8070',
        background: 'white'
    },
    /*
    ** Build configuration
    */
    build: {
        extractCss: true,
        /*
        ** Run ESLint on save
        */
        extend(config, {isDev, isClient}) {
            if (isDev && isClient) {
                config.module.rules.push({
                    enforce: 'pre',
                    test: /\.(js|vue)$/,
                    loader: 'eslint-loader',
                    exclude: /(node_modules)/
                })
            }
        }
    },
    css: [
        // SCSS file in the project
        '~/node_modules/@fortawesome/fontawesome-free/css/all.css',
        '~/assets/scss/main.scss'
    ],

    auth: {
        strategies: {
            local: {
                scheme: 'refresh',
                token: {
                    maxAge: 60
                },
                refreshToken: {
                    maxAge: 60 * 60 * 24 * 14
                },
                endpoints: {
                    login: {
                        url: 'auth/login', method: 'post', propertyName: 'token'
                    },
                    user: {
                        url: 'me', method: 'get', propertyName: 'data'
                    },
                    logout: {
                        url: 'auth/logout', method: 'get'
                    }
                }
            }
        },
        redirect: {
            login: '/auth/login',
            logout: '/auth/login',
            home: '/'
        }
    },

    router: {
        middleware: [
            'auth',
            'clearValidationErrors',
        ]
    },

    plugins: [
        {src: './plugins/vue-notification-ssr', mode: 'server'},
        {src: './plugins/vue-notification-client', mode: 'client'},
        './plugins/mixins/validation',
        './plugins/mixins/editor',
        './plugins/mixins/user',
        './plugins/axios',
        './plugins/directives'
    ],

    modules: [
        'bootstrap-vue/nuxt',
        '@nuxtjs/axios',
        '@nuxtjs/auth'
    ],

    bootstrapVue: {
        bootstrapCSS: false,
        bootstrapVueCSS: false
    },

    axios: {
        progress: true,
        baseURL: 'https://przepisomatv2.lndo.site/api',
        headers: {
            common: {
                'Content-Type': 'application/json',
                'X-Requested-With': 'XMLHttpRequest'
            }
        }
    }
}
