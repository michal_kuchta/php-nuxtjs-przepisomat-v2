export default function ({redirect, $auth}) {
    if (!($auth.user.roles.length > 1 && $auth.user.roles[1] == 'master_admin')) {
        return redirect('/404');
    }
}
