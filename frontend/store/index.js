export const getters = {
    authenticated(state) {
        return state.auth ? state.auth.loggedIn : false;
    },
    user(state) {
        return state.auth ? state.auth.user : {};
    }
};
