export const state = () => ({
    editorApiKey: 'cg3bnhh4mjkf0n20d1x36s980d8ynts6df7fvixhye72x6gf',
    editorConfig: {
        plugins: [
            'advlist autolink lists link image charmap hr anchor pagebreak',
            'searchreplace wordcount visualblocks visualchars code","insertdatetime media nonbreaking save table contextmenu directionality',
            'emoticons template paste textcolor colorpicker textpattern autosave'
        ],
        toolbar: 'undo redo | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image',
        relative_urls: false,
        paste_data_images: false,
        browser_spellcheck: true,
        entity_encoding: "raw",
        autoresize_bottom_margin: 0,
        nowrap: false,
        resize: false,
        language: 'pl',
        language_url: "/js/langs/tinymce_pl.js",
        height: 210
    }
});

export const getters = {
    editorConfig(state) {
        return state.editorConfig;
    },
    editorApiKey(state) {
        return state.editorApiKey;
    }
};

