<?php


namespace App\Extensions\Responses;


class ApiResponses
{
    /**
     * @OA\Schema(
     *     schema="NotFoundError",
     *     @OA\Property(
     *         property="error",
     *         type="string"
     *     ),
     *     @OA\Property(
     *         property="type",
     *         type="string"
     *     )
     * )
     * @param string $error
     * @param string $type
     * @return \Illuminate\Http\JsonResponse
     */
    public static function objectNotFound($error = '', $type = 'error')
    {
        return response()->json([
            "error" => $error,
            'type' => $type
        ], 404);
    }

    /**
     * @OA\Schema(
     *     schema="Success",
     * )
     * @param string $error
     * @return \Illuminate\Http\JsonResponse
     */
    public static function success($code = 200)
    {
        return response()->json([], $code);
    }

    /**
     * @OA\Schema(
     *     schema="ValidationErrors",
     *     @OA\Property(
     *         property="error",
     *         type="string"
     *     ),
     *     @OA\Property(
     *          property="errors",
     *          type="array",
     *          @OA\Items(
     *              type="string"
     *          )
     *     ),
     *     @OA\Property(
     *         property="type",
     *         type="string"
     *     )
     * )
     * @param string $error
     * @param array $errors
     * @param string $type
     * @return \Illuminate\Http\JsonResponse
     */
    public static function validationErrors($error = '', $errors = [], $type = 'error')
    {
        return response()->json([
            'error' => $error,
            'errors' => $errors,
            'type' => $type
        ], 406);
    }

    /**
     * @OA\Schema(
     *     schema="ForbiddenError",
     *     @OA\Property(
     *         property="error",
     *         type="string"
     *     ),
     *     @OA\Property(
     *         property="type",
     *         type="string"
     *     )
     * )
     * @param string $error
     * @param string $type
     * @return \Illuminate\Http\JsonResponse
     */
    public static function forbidden($error, $type = 'error')
    {
        return response()->json([
            "error" => $error,
            'type' => $type
        ], 403);
    }
}