<?php

use Illuminate\Support\Facades\Log;

/**
 * Returns the translation of a string.
 *
 * @param string $original
 *
 * @return string
 */
function __($original)
{
    $text = trans($original);

    if (func_num_args() === 1)
    {
        return $text;
    }

    $args = array_slice(func_get_args(), 1);
    return vsprintf($text, is_array($args[0]) ? $args[0] : $args);
}


if (!function_exists('dump')) {
    /**
     * @author Nicolas Grekas <p@tchwork.com>
     */
    function dump(...$moreVars)
    {
        foreach ($moreVars as $v) {
            if (is_array($v) || is_object($v))
                $v = json_encode($v);

            Log::channel('debug')->debug($v);
        }
    }
}

if (!function_exists('dd')) {
    function dd(...$vars)
    {
        dump($vars);
    }
}
