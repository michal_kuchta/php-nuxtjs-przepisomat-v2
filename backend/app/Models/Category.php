<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Category extends Model
{
    protected $table = 'categories';

    public $timestamps = true;

    protected $fillable = [
        'name',
        'parent_id',
        'slug',
        'ancestors',
        'level',
        'position',
        'is_active',
        'has_children'
    ];

    public function getPerPage()
    {
        return 10;
    }

    /**
     * @param $query
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public static function findQuery($query = '')
    {
        $query = preg_replace("/[^A-Za-z0-9 ]/", '', $query);
        $sql = parent::query();

        if (!empty($query))
            $sql->whereRaw('LOWER(name) LIKE "%' . $query . '%"')
                ->whereRaw('LOWER(slug) LIKE "%' . $query . '%"');

        return $sql;
    }

    public function children()
    {
        return $this->hasMany(Category::class, 'parent_id', 'id');
    }
}
