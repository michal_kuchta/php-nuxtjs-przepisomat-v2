<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * @OA\Schema(
 *      @OA\Xml(
 *          name="Photo"
 *      )
 * )
 * Class Photo
 * @package App\Models
 */
class Photo extends Model
{
    /**
     * @OA\Property(
     *      property="id",
     *      type="integer"
     * )
     * @OA\Property(
     *      property="url",
     *      type="string"
     * )
     */

    protected $table = 'photos';

    public $timestamps = true;

    protected $fillable = [
        'url',
    ];

    public function getPerPage()
    {
        return 10;
    }
}
