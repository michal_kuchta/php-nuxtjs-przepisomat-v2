<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * @OA\Schema(
 *      @OA\Property(
 *          property="id",
 *          type="integer"
 *      ),
 *      @OA\Property(
 *          property="recipe_id",
 *          type="integer"
 *      ),
 *      @OA\Property(
 *          property="description",
 *          type="string"
 *      ),
 *      @OA\Property(
 *          property="photo_id",
 *          type="number",
 *          format="integer"
 *      ),
 *      @OA\Property(
 *          property="position",
 *          type="integer"
 *      ),
 *      @OA\Property(
 *          property="photo",
 *          type="object",
 *          ref="#/components/schemas/Photo"
 *      )
 * )
 *
 * Class RecipeStep
 * @package App\Models
 */
class RecipeStep extends Model
{
    protected $table = 'recipe_steps';

    public $timestamps = false;

    protected $fillable = [
        'recipe_id',
        'position',
        'description',
        'photo_id',
    ];

    public function getPerPage()
    {
        return 10;
    }

    public function photo()
    {
        return $this->hasOne(Photo::class, 'id', 'photo_id');
    }
}
