<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * Class CartItem
 * @package App\Models
 */
class CartItem extends Model
{
    protected $table = 'cart_items';

    public $timestamps = true;

    protected $fillable = [
        'recipe_id',
        'ingredient_id',
        'user_id',
    ];

    public function recipe()
    {
        return $this->hasOne(Recipe::class, 'id', 'recipe_id');
    }

    public function ingredient(){
        return $this->hasOne(RecipeIngredient::class, 'id', 'ingredient_id');
    }

    public function user(){
        return $this->hasOne(User::class, 'id', 'user_id');
    }
}