<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * @OA\Schema(
 *      @OA\Xml(
 *          name="Unit"
 *      )
 * )
 * Class Unit
 * @package App\Models
 */
class Unit extends Model
{
    /**
     * @OA\Property(
     *      property="id",
     *      type="integer"
     * )
     * @OA\Property(
     *      property="name",
     *      type="string"
     * )
     * @OA\Property(
     *      property="multiplicity",
     *      type="float"
     * )
     * @OA\Property(
     *      property="abbr_quantity_one",
     *      type="string"
     * )
     * @OA\Property(
     *      property="abbr_quantity_float",
     *      type="string"
     * )
     * @OA\Property(
     *      property="abbr_quantity_two_four",
     *      type="string"
     * )
     * @OA\Property(
     *      property="abbr_quantity_five_and_more",
     *      type="string"
     * )
     * @OA\Property(
     *      property="child",
     *      type="object",
     *      description="Unit schema",
     * )
     * @OA\Property(
     *      property="parent",
     *      type="object",
     *      description="Unit schema",
     * )
     * @var string
     */

    protected $table = 'units';

    public $timestamps = false;

    protected $fillable = [
        'name',
        'child_id',
        'parent_id',
        'multiplicity',
        'abbr_quantity_one',
        'abbr_quantity_float',
        'abbr_quantity_two_four',
        'abbr_quantity_five_and_more',
    ];

    public function getPerPage()
    {
        return 10;
    }

    public function child()
    {
        return $this->hasOne(Unit::class, 'id', 'child_id');
    }

    public function parent()
    {
        return $this->belongsTo(Unit::class, 'id', 'child');
    }

    public function getBaseMultiplicity()
    {
        if ($this->child_id > 0)
        {
            return ($this->multiplicity > 0 ? $this->multiplicity : 1) * $this->child->getBaseMultiplicity();
        }

        return $this->multiplicity > 0 ? $this->multiplicity : 1;
    }

    public function getBaseUnit()
    {
        if ($this->child_id > 0)
        {
            return $this->child->getBaseUnit();
        }

        return $this;
    }
}
