<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * Class Recipe
 * @package App\Models
 */
class RecipeIngredient extends Model
{
    protected $table = 'recipe_ingredients';

    public $timestamps = false;

    protected $fillable = [
        'recipe_id',
        'ingredient_id',
        'value',
        'unit_id',
    ];

    public function getPerPage()
    {
        return 10;
    }

    public function ingredient()
    {
        return $this->hasOne(Ingredient::class, 'id', 'ingredient_id');
    }

    public function unit(){
        return $this->hasOne(Unit::class, 'id', 'unit_id');
    }

    public function calculateValueToBaseUnit()
    {
        return $this->value * $this->unit->getBaseMultiplicity();
    }
}
