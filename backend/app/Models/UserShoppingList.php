<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class UserShoppingList extends Model
{
    protected $table = 'user_shopping_list';

    public $timestamps = false;

    protected $fillable = [
        'user_id',
        'ingredient_id',
        'total',
        'unit_id',
    ];

    public function getPerPage()
    {
        return 10;
    }
}
