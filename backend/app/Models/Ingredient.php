<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * @OA\Schema(
 *      @OA\Xml(
 *          name="Ingredient"
 *      ),
 *      @OA\Property(
 *          property="id",
 *          type="integer"
 *      ),
 *      @OA\Property(
 *          property="name",
 *          type="string"
 *      )
 * )
 * @package App\Models
 */
class Ingredient extends Model
{
    protected $table = 'ingredients';

    public $timestamps = true;

    protected $fillable = [
        'name',
    ];

    public function getPerPage()
    {
        return 10;
    }
}
