<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * Class Recipe
 * @package App\Models
 */
class Recipe extends Model
{
    const SHARE_TYPE_INHERIT = -1;
    const SHARE_TYPE_NONE = 0;
    const SHARE_TYPE_MAILS = 1;
    const SHARE_TYPE_ALL = 2;

    protected $table = 'recipes';

    public $timestamps = true;

    protected $fillable = [
        'name',
        'description',
        'user_id',
        'portions',
        'estimated_price',
        'preparing_time',
        'difficulty',
        'category_id',
        'rating_count',
        'rating_value',
        'share_type',
    ];

    protected static function boot()
    {
        parent::boot();

        self::observe(new RecipeObserver());
    }

    /**
     * @param string $query
     * @param int $categoryId
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public static function findQuery($query = '', $categoryId = 0)
    {
        $query = preg_replace("/[^A-Za-z0-9 ]/", '', $query);
        $sql = parent::query();

        if (!empty($query))
            $sql = $sql->whereRaw('LOWER(name) LIKE "%' . $query . '%"');

        if ($categoryId > 0)
        {
            $sql = $sql->where('category_id', $categoryId);
        }

        if (auth()->user()->is_admin)
        {
            return $sql;
        }

        return $sql->where(function(\Illuminate\Database\Eloquent\Builder $sql){
            $sql->orWhere('user_id', auth()->id())
                ->orWhereIn('id', RecipeShare::query()
                    ->orWhere(function (\Illuminate\Database\Eloquent\Builder $sql){
                        $sql->where('type', Recipe::SHARE_TYPE_MAILS)
                            ->where('email', auth()->user()->email);
                    })
                    ->orWhere('type', Recipe::SHARE_TYPE_ALL)
                    ->select('object_id')
                )
                ->orWhere(function (\Illuminate\Database\Eloquent\Builder $sql){
                    $sql->whereIn('user_id', UserShare::query()
                        ->orWhere('type', Recipe::SHARE_TYPE_ALL)
                        ->orWhere(function(\Illuminate\Database\Eloquent\Builder $sql){
                            $sql->where('type', Recipe::SHARE_TYPE_MAILS)->where('email', auth()->user()->email);
                        })
                        ->select('object_id')
                    )->where('share_type', Recipe::SHARE_TYPE_INHERIT);
                });
        });
    }

    public function getPerPage()
    {
        return 10;
    }

    public function category(){
        return $this->hasOne(Category::class, 'id', 'category_id');
    }

    public function categories(){
        return $this->hasManyThrough(Category::class, RecipeCategory::class, 'category_id', 'id', 'id', 'recipe_id');
    }

    public function ingredients(){
        return $this->hasMany(RecipeIngredient::class, 'recipe_id', 'id');
    }

    public function photos(){
        return $this->hasMany(RecipePhoto::class, 'recipe_id', 'id');
    }

    public function subRecipes(){
        return $this->hasManyThrough(Recipe::class, RecipeRecipe::class, 'parent_id', 'id', 'id', 'recipe_id');
    }

    public function steps(){
        return $this->hasMany(RecipeStep::class, 'recipe_id', 'id');
    }

    public function shares()
    {
        return $this->hasMany(RecipeShare::class, 'object_id', 'id');
    }

    public function rates()
    {
        return $this->hasMany(RecipeRate::class, 'recipe_id', 'id');
    }
}

class RecipeObserver
{
    public function creating(Recipe $model)
    {
        if (empty($model->portions) || intval($model->portions) == 0)
            $model->portions = 1;

        if (empty($model->preparing_time) || intval($model->preparing_time) == 0)
            $model->preparing_time = 1;

        if (empty($model->estimated_price) || is_null($model->estimated_price))
            $model->estimated_price = 0;
    }

    public function updating(Recipe $model)
    {
        $this->creating($model);
    }
}
