<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class UserShare extends Model
{
    protected $table = 'user_shares';

    public $timestamps = true;

    protected $fillable = [
        'email',
        'type',
        'object_id',
    ];

    public function getPerPage()
    {
        return 10;
    }
}
