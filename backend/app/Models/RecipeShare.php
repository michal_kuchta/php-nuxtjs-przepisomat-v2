<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class RecipeShare extends Model
{
    protected $table = 'recipe_shares';

    public $timestamps = true;

    protected $fillable = [
        'email',
        'type',
        'object_id',
    ];

    public function getPerPage()
    {
        return 10;
    }
}
