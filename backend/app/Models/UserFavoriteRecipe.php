<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class UserFavoriteRecipe extends Model
{
    protected $table = 'user_favorite_recipes';

    public $timestamps = false;

    protected $fillable = [
        'user_id',
        'recipe_id',
    ];

    public function getPerPage()
    {
        return 10;
    }
}
