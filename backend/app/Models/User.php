<?php

namespace App\Models;

use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Str;
use Tymon\JWTAuth\Contracts\JWTSubject;

class User extends Authenticatable implements JWTSubject
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password', 'is_rodo_checked', 'is_actve', 'is_admin', 'share_type'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token', 'is_active', 'is_admin'
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    public function getPerPage()
    {
        return 10;
    }

    /**
     * Get the identifier that will be stored in the subject claim of the JWT.
     *
     * @return mixed
     */
    public function getJWTIdentifier()
    {
        return $this->getKey();
    }

    /**
     * Return a key value array, containing any custom claims to be added to the JWT.
     *
     * @return array
     */
    public function getJWTCustomClaims()
    {
        return [];
    }

    public function favorites()
    {
        return $this->hasMany(UserFavoriteRecipe::class, 'user_id', 'id');
    }

    public function cartItems()
    {
        return $this->hasMany(CartItem::class, 'user_id', 'id');
    }

    public function cartItemsCount()
    {
        return CartItem::query()->where('user_id', auth()->id())->groupBy('user_id', 'recipe_id', 'ingredient_id')->selectRaw('count(*)')->get()->count();
    }

    public function sharedEmails()
    {
        return $this->hasMany(UserShare::class, 'object_id', 'id');
    }

    public function anonymize()
    {
        UserShare::query()->where('object_id', $this->id)->delete();
        UserShoppingList::query()->where('user_id', $this->id)->delete();
        UserFavoriteRecipe::query()->where('user_id', $this->id)->delete();
        CartItem::query()->where('user_id', $this->id)->delete();
        Photo::query()->where('user_id', $this->id)->delete();
        RecipeCategory::query()->whereIn('recipe_id', Recipe::query()->where('user_id', $this->id)->select('id'))->delete();
        RecipeIngredient::query()->whereIn('recipe_id', Recipe::query()->where('user_id', $this->id)->select('id'))->delete();
        Ingredient::query()->where('user_id', $this->id)->delete();
        RecipePhoto::query()->whereIn('recipe_id', Recipe::query()->where('user_id', $this->id)->select('id'))->delete();
        RecipeRate::query()->orWhereIn('recipe_id', Recipe::query()->where('user_id', $this->id)->select('id'))->orWhere('user_id', $this->id)->delete();
        RecipeRecipe::query()->whereIn('parent_id', Recipe::query()->where('user_id', $this->id)->select('id'))->delete();
        RecipeShare::query()->whereIn('object_id', Recipe::query()->where('user_id', $this->id)->select('id'))->delete();
        RecipeStep::query()->whereIn('recipe_id', Recipe::query()->where('user_id', $this->id)->select('id'))->delete();
        Recipe::query()->where('user_id', $this->id)->delete();

        $this->is_active = false;
        $this->is_rodo_checked = false;
        $this->share_type = 0;
        $this->is_admin = false;
        $this->password = Hash::make(Str::random());
        $this->email = 'anonymized_'.$this->id.'@przepisomat.pl';
        $this->name = 'anonymized user ' . $this->id;
        $this->save();
    }
}
