<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * @OA\Schema(
 *      @OA\Property(
 *          property="id",
 *          type="integer"
 *      ),
 *      @OA\Property(
 *          property="recipe_id",
 *          type="integer"
 *      ),
 *      @OA\Property(
 *          property="photo_id",
 *          type="integer"
 *      ),
 *      @OA\Property(
 *          property="photo",
 *          type="object",
 *          ref="#/components/schemas/Photo"
 *      ),
 * )
 *
 * Class RecipePhoto
 * @package App\Models
 */
class RecipePhoto extends Model
{
    protected $table = 'recipe_photos';

    public $timestamps = false;

    protected $fillable = [
        'recipe_id',
        'photo_id',
    ];

    public function getPerPage()
    {
        return 10;
    }

    public function photo()
    {
        return $this->hasOne(Photo::class, 'id', 'photo_id');
    }
}
