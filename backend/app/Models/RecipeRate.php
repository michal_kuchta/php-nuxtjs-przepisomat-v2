<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class RecipeRate extends Model
{
    protected $table = 'recipe_rates';

    public $timestamps = false;

    protected $fillable = [
        'user_id',
        'recipe_id',
        'value',
    ];
}
