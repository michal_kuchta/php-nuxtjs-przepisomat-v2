<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class RecipeCategory extends Model
{
    protected $table = 'recipe_categories';

    public $timestamps = false;

    protected $fillable = [
        'recipe_id',
        'category_id',
    ];

    public function getPerPage()
    {
        return 10;
    }
}
