<?php

namespace App\Console\Commands;

use Illuminate\Foundation\Console\ObserverMakeCommand;

class ObserverMake extends ObserverMakeCommand
{
    /**
     * Prefix default root namepsace with a folder.
     *
     * @param string $rootNamespace
     * @return string
     */
    protected function getDefaultNamespace($rootNamespace)
    {
        return $rootNamespace.'\\Models';
    }
}
