<?php

namespace App\Http\Resources;

use App\Models\Recipe;
use App\Models\User;
use App\Models\UserShare;
use Illuminate\Http\Resources\Json\JsonResource;

class UserResource extends JsonResource
{
    /**
     * @OA\Schema(
     *      schema="User",
     *      @OA\Property(
     *          property="id",
     *          type="integer"
     *      ),
     *      @OA\Property(
     *          property="name",
     *          type="string"
     *      ),
     *      @OA\Property(
     *          property="email",
     *          type="string"
     *      ),
     *      @OA\Property(
     *          property="is_rodo_checked",
     *          type="boolean"
     *      ),
     *      @OA\Property(
     *          property="is_actve",
     *          type="boolean"
     *      ),
     *      @OA\Property(
     *          property="share_type",
     *          type="integer"
     *      ),
     *      @OA\Property(
     *          property="favorites_count",
     *          type="integer"
     *      ),
     *      @OA\Property(
     *          property="cart_count",
     *          type="integer"
     *      ),
     *      @OA\Property(
     *          property="shared_emails",
     *          type="array",
     *          @OA\Items(
     *              @OA\Property(
     *                  property="value",
     *                  type="string"
     *              )
     *          )
     *      ),
     *      @OA\Property(
     *          property="roles",
     *          type="array",
     *          @OA\Items(
     *              @OA\Property(
     *                  property="value",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     *
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        /** @var User $user */
        $user = $this->resource;

        return array_merge(parent::toArray($request), [
            'favorites_count' => $user->favorites->count(),
            'cart_count' => $user->cartItemsCount(),
            'shared_emails' => $user->sharedEmails->filter(function (UserShare $share){
                return $share->type == Recipe::SHARE_TYPE_MAILS;
            })->map(function(UserShare $share){
                return ['value' => $share->email];
            })->toArray(),
            'roles' => array_filter(['user', $user->is_admin ? 'master_admin' : null])
        ]);
    }
}
