<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\ResourceCollection;

class CartCollection extends ResourceCollection
{
    /**
     * @OA\Schema(
     *      schema="Cart",
     *      @OA\Property(
     *          property="recipes",
     *          type="array",
     *          @OA\Items(
     *              @OA\Property(
     *                  property="id",
     *                  type="integer"
     *              ),
     *              @OA\Property(
     *                  property="name",
     *                  type="string"
     *              ),
     *              @OA\Property(
     *                  property="ingredients",
     *                  type="array",
     *                  @OA\Items(
     *                      @OA\Property(
     *                          property="id",
     *                          type="integer"
     *                      ),
     *                      @OA\Property(
     *                          property="name",
     *                          type="string"
     *                      ),
     *                      @OA\Property(
     *                          property="value",
     *                          type="number"
     *                      ),
     *                      @OA\Property(
     *                          property="total",
     *                          type="number"
     *                      ),
     *                      @OA\Property(
     *                          property="quantity",
     *                          type="integer"
     *                      ),
     *                      @OA\Property(
     *                          property="unit",
     *                          type="object",
     *                          ref="#/components/schemas/Unit"
     *                      )
     *                  )
     *              )
     *          )
     *      ),
     *      @OA\Property(
     *          property="sum",
     *          type="object",
     *          @OA\Property(
     *              property="ingredients",
     *              type="array",
     *              @OA\Items(
     *                  @OA\Property(
     *                      property="name",
     *                      type="string"
     *                  ),
     *                  @OA\Property(
     *                      property="value",
     *                      type="number"
     *                  ),
     *                  @OA\Property(
     *                      property="unit",
     *                      type="object",
     *                      ref="#/components/schemas/Unit"
     *                  )
     *              )
     *          )
     *      )
     * )
     * Transform the resource collection into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        $recipes = [];
        $ingredients = [];

        foreach ($this->collection as $cartItem)
        {
            if (!isset($recipes[$cartItem->recipe_id]))
            {
                $recipes[$cartItem->recipe_id] = [
                    'id' => $cartItem->recipe_id,
                    'name' => $cartItem->recipe->name,
                    'ingredients' => []
                ];
            }

            if (!isset($recipes[$cartItem->recipe_id]['ingredients'][$cartItem->ingredient_id]))
            {
                $recipes[$cartItem->recipe_id]['ingredients'][$cartItem->ingredient_id] = [
                    'id' => $cartItem->ingredient_id,
                    'name' => $cartItem->ingredient->ingredient->name,
                    'value' => $cartItem->ingredient->value,
                    'quantity' => 1,
                    'total' => $cartItem->ingredient->value,
                    'unit' => $cartItem->ingredient->unit
                ];
            }
            else
            {
                $recipes[$cartItem->recipe_id]['ingredients'][$cartItem->ingredient_id]['quantity']++;
                $recipes[$cartItem->recipe_id]['ingredients'][$cartItem->ingredient_id]['total'] += $cartItem->ingredient->value;
            }

            if (!isset($ingredients[$cartItem->ingredient->ingredient->id]))
            {
                $ingredients[$cartItem->ingredient->ingredient->id] = [
                    'name' => $cartItem->ingredient->ingredient->name,
                    'value' => $cartItem->ingredient->calculateValueToBaseUnit(),
                    'unit' => $cartItem->ingredient->unit->getBaseUnit()
                ];
            }
            else
            {
                $ingredients[$cartItem->ingredient->ingredient->id]['value'] += $cartItem->ingredient->calculateValueToBaseUnit();
            }
        }

        foreach ($recipes as $key =>  $recipe)
        {
            ksort($recipe['ingredients']);
            $recipes[$key] = $recipe;
        }

        ksort($recipes);
        ksort($ingredients);

        return [
            'recipes' => array_values($recipes),
            'sum' => [
                'ingredients' => array_values($ingredients)
            ]
        ];
    }
}
