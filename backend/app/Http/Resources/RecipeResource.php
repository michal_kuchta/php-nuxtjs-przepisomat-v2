<?php

namespace App\Http\Resources;

use App\Models\Recipe;
use App\Models\RecipeShare;
use App\Models\UserFavoriteRecipe;
use Illuminate\Http\Resources\Json\JsonResource;

class RecipeResource extends JsonResource
{
    /**
     * @OA\Schema(
     *     schema="Recipe",
     *      @OA\Property(
     *          property="id",
     *          type="integer"
     *      ),
     *      @OA\Property(
     *          property="name",
     *          type="string"
     *      ),
     *      @OA\Property(
     *          property="description",
     *          type="string"
     *      ),
     *      @OA\Property(
     *          property="user_id",
     *          type="integer"
     *      ),
     *      @OA\Property(
     *          property="portions",
     *          type="integer"
     *      ),
     *      @OA\Property(
     *          property="estimated_price",
     *          type="integer"
     *      ),
     *      @OA\Property(
     *          property="preparing_time",
     *          type="integer"
     *      ),
     *      @OA\Property(
     *          property="difficulty",
     *          type="integer"
     *      ),
     *      @OA\Property(
     *          property="category_id",
     *          type="integer"
     *      ),
     *      ref="#/components/schemas/RecipeRate",
     *      @OA\Property(
     *          property="share_type",
     *          type="integer"
     *      ),
     *      @OA\Property(
     *          property="shared_emails",
     *          type="array",
     *          @OA\Items(
     *              @OA\Property(
     *                  property="value",
     *                  type="string"
     *              )
     *          )
     *      ),
     *      @OA\Property(
     *          property="photo",
     *          type="object",
     *          ref="#/components/schemas/Photo"
     *      ),
     *      @OA\Property(
     *          property="is_favourite",
     *          type="boolean"
     *      ),
     *      @OA\Property(
     *          property="is_author",
     *          type="boolean"
     *      )
     * )
     *
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return array_merge(parent::toArray($request), [
            'shared_emails' => $this->resource->shares->filter(function (RecipeShare $share){
                return $share->type == Recipe::SHARE_TYPE_MAILS;
            })->map(function(RecipeShare $share){
                return ['value' => $share->email];
            })->toArray(),
            'photo' => new PhotoResource($this->resource->photos->first() ? $this->resource->photos->first()->photo : null),
            'is_favourite' => UserFavoriteRecipe::query()->where('user_id', auth()->id())->where('recipe_id', $this->resource->id)->exists(),
            'is_author' => auth()->id() === $this->resource->user_id
        ], (new RecipeRateResource($this->resource))->toArray($request));
    }
}
