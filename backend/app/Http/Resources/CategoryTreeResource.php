<?php

namespace App\Http\Resources;

use App\Models\Category;
use App\Models\Recipe;
use App\Models\RecipeShare;
use App\Models\UserFavoriteRecipe;
use Illuminate\Http\Resources\Json\JsonResource;
use Illuminate\Support\Str;

class CategoryTreeResource extends JsonResource
{
    public static $type = 'list';

    /**
     * @OA\Schema(
     *     schema="CategoryTree",
     *      @OA\Property(
     *          property="id",
     *          type="integer"
     *      ),
     *      @OA\Property(
     *          property="name",
     *          type="string"
     *      ),
     *      @OA\Property(
     *          property="parent_id",
     *          type="integer"
     *      ),
     *      @OA\Property(
     *          property="slug",
     *          type="string"
     *      ),
     *      @OA\Property(
     *          property="level",
     *          type="integer"
     *      ),
     *      @OA\Property(
     *          property="position",
     *          type="integer"
     *      ),
     *      @OA\Property(
     *          property="has_children",
     *          type="boolean"
     *      ),
     *      @OA\Property(
     *          property="children",
     *          type="array",
     *          @OA\Items()
     *      ),
     *      @OA\Property(
     *          property="ancestors",
     *          type="array",
     *          @OA\Items()
     *      )
     * )
     *
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return array_merge(parent::toArray($request), [
            'parent_id' => intval($this->resource->parent_id),
            'children' => $this->resource->children->map(function (Category $model){
                return (new self($model));
            }),
            'ancestors' => array_filter(explode('#', $this->resource->ancestors))
        ]);
    }
}
