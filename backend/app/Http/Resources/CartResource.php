<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class CartResource extends JsonResource
{
    /**
     * @OA\Schema(
     *      schema="CartItem",
     *      @OA\Property(
     *          property="id",
     *          type="integer"
     *      ),
     *      @OA\Property(
     *          property="recipe_id",
     *          type="integer"
     *      ),
     *      @OA\Property(
     *          property="ingredient_id",
     *          type="integer"
     *      ),
     *      @OA\Property(
     *          property="value",
     *          type="number",
     *          format="double"
     *      ),
     *      @OA\Property(
     *          property="unit_id",
     *          type="integer"
     *      ),
     *      @OA\Property(
     *          property="unit",
     *          type="object",
     *          ref="#/components/schemas/Unit"
     *      ),
     *      @OA\Property(
     *          property="ingredient",
     *          type="object",
     *          ref="#/components/schemas/Ingredient"
     *      )
     * )
     *
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->resource->id,
            'recipe' => [
                'id' => $this->resource->recipe_id,
                'name' => $this->resource->recipe->name
            ],
            'ingredient' => [
                'id' => $this->resource->ingredient_id,
                'name' => $this->resource->ingredient->ingredient->name,
                'unit' => $this->resource->ingredient->unit
            ]
        ];
    }
}
