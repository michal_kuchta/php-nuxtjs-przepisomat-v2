<?php

namespace App\Http\Resources;

use App\Models\Recipe;
use App\Models\RecipeShare;
use App\Models\UserFavoriteRecipe;
use Illuminate\Http\Resources\Json\JsonResource;

class RecipeRateResource extends JsonResource
{
    /**
     * @OA\Schema(
     *      schema="RecipeRate",
     *      @OA\Property(
     *          property="rate",
     *          type="object",
     *          @OA\Property(
     *              property="value",
     *              type="number"
     *          ),
     *          @OA\Property(
     *              property="count",
     *              type="integer"
     *          ),
     *          @OA\Property(
     *              property="user_value",
     *              type="integer"
     *          ),
     *      )
     * )
     *
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        $userValue = $this->resource->rates->where('user_id', auth()->id())->first();

        return [
            'rate' => [
                'value' => number_format($this->resource->rates->average('value'), 2),
                'count' => $this->resource->rates->count(),
                'user_value' => $userValue ? $userValue->value : 0
            ]
        ];
    }
}
