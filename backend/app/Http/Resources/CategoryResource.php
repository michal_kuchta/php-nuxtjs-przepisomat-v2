<?php

namespace App\Http\Resources;

use App\Models\Category;
use App\Models\Recipe;
use App\Models\RecipeShare;
use App\Models\UserFavoriteRecipe;
use Illuminate\Http\Resources\Json\JsonResource;

class CategoryResource extends JsonResource
{
    public static $type = 'list';

    /**
     * @OA\Schema(
     *     schema="Category",
     *      @OA\Property(
     *          property="id",
     *          type="integer"
     *      ),
     *      @OA\Property(
     *          property="name",
     *          type="string"
     *      ),
     *      @OA\Property(
     *          property="parent_id",
     *          type="integer"
     *      ),
     *      @OA\Property(
     *          property="slug",
     *          type="string"
     *      ),
     *      @OA\Property(
     *          property="level",
     *          type="integer"
     *      ),
     *      @OA\Property(
     *          property="position",
     *          type="integer"
     *      ),
     *      @OA\Property(
     *          property="has_children",
     *          type="boolean"
     *      )
     * )
     *
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return array_merge(parent::toArray($request), [
            'parent_id' => intval($this->resource->parent_id),
        ]);
    }
}
