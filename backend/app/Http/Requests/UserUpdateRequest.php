<?php

namespace App\Http\Requests;

use App\Http\Requests\DefaultApiRequest;
use Illuminate\Validation\Rule;

class UserUpdateRequest extends DefaultApiRequest
{
    /**
     * @OA\Schema(
     *     schema="UserUpdateRequest",
     *     required={"name", "email", "password"},
     *     @OA\Property(
     *          property="name",
     *          type="string"
     *     ),
     *     @OA\Property(
     *          property="email",
     *          type="string"
     *     ),
     *     @OA\Property(
     *          property="password",
     *          type="string"
     *     ),
     *     @OA\Property(
     *          property="is_rodo_checked",
     *          type="boolean"
     *     )
     * )
     */

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => ['required', 'string', 'max:255'],
            'email' => ['required', 'string', 'email', 'max:255', Rule::unique('users', 'email')->ignore(auth()->user()->id)],
            'password' => ['required', 'string', 'min:5'],
            'is_rodo_checked' => ['required']
        ];
    }

    /**
     * Get custom messages for validator errors.
     *
     * @return array
     */
    public function messages()
    {
        return [
            'name.required' => __('Imię i Nazwisko jest wymagane'),
            'name.string' => __('Niepoprawna wartość pola Imię i Nazwisko'),
            'name.max' => __('Wartość pola Imię i nazwisko jest zbyt długa'),
            'email.required' => __('Adres e-mail jest wymagany'),
            'email.string' => __('Adres e-mail jest nieprawidłowy lub juz istnieje konto o takim adresie'),
            'email.email' => __('Adres e-mail jest nieprawidłowy lub juz istnieje konto o takim adresie'),
            'email.max' => __('Zbyt długi adres e-mail'),
            'email.unique:users' => __('Adres e-mail jest nieprawidłowy lub juz istnieje konto o takim adresie'),
            'password.required' => __('Hasło jest wymagane'),
            'password.string' => __('Niepoprawne hasło'),
            'password.min' => __('Hasło powinno składać się z minimum :min znaków'),
            'is_rodo_checked.required' => __('Zaznaczenie zgody jest wymagane'),
        ];
    }
}
