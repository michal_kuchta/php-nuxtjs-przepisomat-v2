<?php

namespace App\Http\Requests;

use App\Http\Requests\DefaultApiRequest;

class UnitStoreRequest extends DefaultApiRequest
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => 'required',
            'child' => $this->get('child') > 0 ? ['sometimes', 'exists:units,id'] : [],
            'parent_id' => $this->get('parent_id') > 0 ? ['sometimes', 'exists:units,id'] : [],
            'multiplicity' => ['required', 'min:0'],
            'abbr_quantity_one' => 'required',
            'abbr_quantity_float' => 'required',
            'abbr_quantity_two_four' => 'required',
            'abbr_quantity_five_and_more' => 'required',
        ];
    }

    /**
     * Get custom messages for validator errors.
     *
     * @return array
     */
    public function messages()
    {
        return [
            'name.required' => __('Pole nazwa jest wymagane'),
            'child.exists' => __('Id dziecka nie istnieje w bazie danych'),
            'parent_id.exists' => __('Id rodzica nie istnieje w bazie danych'),
            'multiplicity.required' => __('Wielokrotność jest wymagana'),
            'multiplicity.min' => __('Wielokrotność nie może byc mniejsza od zera'),
            'abbr_quantity_one.required' => __('Skrót dla ilości 1 jest wymagany'),
            'abbr_quantity_float.required' => __('Skrót dla ilości zmiennoprzecinkowej jest wymagany'),
            'abbr_quantity_two_four.required' => __('Skrót dla ilości większej bądź równej 2 oraz mniejszej bądź równej 4 jest wymagany'),
            'abbr_quantity_five_and_more.required' => __('Skrót dla ilości większej bądź równej 5 jest wymagany'),
        ];
    }
}
