<?php

namespace App\Http\Requests;

use App\Http\Requests\DefaultApiRequest;

class RecipeStepStoreRequest extends DefaultApiRequest
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'position' => ['required', 'min:1', 'integer'],
        ];
    }

    public function messages()
    {
        return [
            'position.required' => __('Pozycja kroku jest wymagana'),
            'position.min' => __('Pozycja kroku musi być większ od 1'),
            'position.integer' => __('Pozycja kroku musi być liczbą'),
        ];
    }
}
