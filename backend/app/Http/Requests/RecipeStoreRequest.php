<?php

namespace App\Http\Requests;

use App\Http\Requests\DefaultApiRequest;

class RecipeStoreRequest extends DefaultApiRequest
{
    /**
     * @OA\Schema(
     *     schema="RecipeRequest",
     *     required={"name"},
     *     @OA\Property(
     *          property="name",
     *          type="string"
     *     ),
     *     @OA\Property(
     *          property="description",
     *          type="string"
     *     ),
     *     @OA\Property(
     *          property="portions",
     *          type="integer"
     *     ),
     *     @OA\Property(
     *          property="preparing_time",
     *          type="integer"
     *     ),
     *     @OA\Property(
     *          property="estimated_price",
     *          type="integer"
     *     ),
     *     @OA\Property(
     *          property="difficulty",
     *          type="integer"
     *     ),
     *     @OA\Property(
     *          property="category_id",
     *          type="integer"
     *     ),
     *     @OA\Property(
     *          property="share_type",
     *          type="integer"
     *     ),
     *     @OA\Property(
     *          property="shared_emails",
     *          type="array",
     *          @OA\Items(
     *              @OA\Property(
     *                  property="value",
     *                  type="string"
     *              )
     *          )
     *     )
     * )
     */

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => 'required'
        ];
    }

    /**
     * Get custom messages for validator errors.
     *
     * @return array
     */
    public function messages()
    {
        return [
            'name.required' => __('Pole Nazwa jest wymagane')
        ];
    }
}
