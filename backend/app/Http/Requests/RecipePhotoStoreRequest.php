<?php

namespace App\Http\Requests;

use App\Http\Requests\DefaultApiRequest;

class RecipePhotoStoreRequest extends DefaultApiRequest
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'photo_id' => ['required', 'exists:photos,id']
        ];
    }

    /**
     * Get custom messages for validator errors.
     *
     * @return array
     */
    public function messages()
    {
        return [
            'photo_id.required' => __("Id zdjęcia jest wymagane"),
            'photo_id.exists' => __("Zdjęcie nie istnieje w bazie danych"),
        ];
    }
}
