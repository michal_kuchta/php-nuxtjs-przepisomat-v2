<?php

namespace App\Http\Requests;

use App\Http\Requests\DefaultApiRequest;

class UserPasswordUpdateRequest extends DefaultApiRequest
{
    /**
     * @OA\Schema(
     *     schema="UserPasswordUpdateRequest",
     *     required={"password", "new_password", "new_password_confirmation"},
     *     @OA\Property(
     *          property="password",
     *          type="string"
     *     ),
     *     @OA\Property(
     *          property="new_password",
     *          type="string"
     *     ),
     *     @OA\Property(
     *          property="new_password_confirmation",
     *          type="string"
     *     )
     * )
     */

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'password' => ['required', 'string', 'min:5'],
            'new_password' => ['required', 'string', 'min:5', 'confirmed'],
        ];
    }

    /**
     * Get custom messages for validator errors.
     *
     * @return array
     */
    public function messages()
    {
        return [
            'password.required' => __('Hasło jest wymagane'),
            'password.string' => __('Niepoprawne hasło'),
            'password.min' => __('Hasło powinno składać się z minimum :min znaków'),
            'new_password.required' => __('Hasło jest wymagane'),
            'new_password.string' => __('Niepoprawne hasło'),
            'new_password.min' => __('Hasło powinno składać się z minimum :min znaków'),
            'new_password.confirmation' => __('Oba nowe hasła powinny być takie same'),
            'is_rodo_checked.required' => __('Zaznaczenie zgody jest wymagane'),
        ];
    }
}
