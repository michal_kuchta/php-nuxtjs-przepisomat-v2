<?php

namespace App\Http\Requests;

use App\Http\Requests\DefaultApiRequest;

class IngredientStoreRequest extends DefaultApiRequest
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => [
                'required',
                'min:1'
            ]
        ];
    }

    public function messages()
    {
        return [
            'name' => __('Pole Nazwa jest wymagane')
        ];
    }
}
