<?php

namespace App\Http\Requests;

use App\Http\Requests\DefaultApiRequest;

class CategoryStoreRequest extends DefaultApiRequest
{
    /**
     * @OA\Schema(
     *     schema="CategoryRequest",
     *     required={"name"},
     *     @OA\Property(
     *          property="name",
     *          type="string"
     *     ),
     *     @OA\Property(
     *          property="parent_id",
     *          type="integer"
     *     ),
     *     @OA\Property(
     *          property="is_active",
     *          type="boolean"
     *     )
     * )
     */

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => 'required',
            'slug' => 'required|unique:categories,slug'
        ];
    }

    /**
     * Get custom messages for validator errors.
     *
     * @return array
     */
    public function messages()
    {
        return [
            'name.required' => __('Pole Nazwa jest wymagane')
        ];
    }
}
