<?php

namespace App\Http\Requests;

use App\Http\Requests\DefaultApiRequest;

class RecipeIngredientStoreRequest extends DefaultApiRequest
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'ingredient_id' => ['required', 'exists:ingredients,id'],
            'unit_id' => ['sometimes', 'exists:photos,id'],
        ];
    }

    public function messages()
    {
        return [
            'ingredient_id' =>  __('Wybrany składnik nie istnieje w bazie danych'),
            'unit_id' =>  __('Wybrana jednostka nie istnieje w bazie danych'),
        ];
    }
}
