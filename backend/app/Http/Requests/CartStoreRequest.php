<?php

namespace App\Http\Requests;

use App\Http\Requests\DefaultApiRequest;

class CartStoreRequest extends DefaultApiRequest
{
    /**
     * @OA\Schema(
     *     schema="CartStoreRequest",
     *     required={"recipe_id", "ingredient_id"},
     *     @OA\Property(
     *          property="recipe_id",
     *          type="integer"
     *     ),
     *     @OA\Property(
     *          property="ingredient_id",
     *          type="integer"
     *     )
     * )
     */

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [];
    }

    /**
     * Get custom messages for validator errors.
     *
     * @return array
     */
    public function messages()
    {
        return [];
    }
}
