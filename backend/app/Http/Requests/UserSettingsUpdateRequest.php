<?php

namespace App\Http\Requests;

use App\Http\Requests\DefaultApiRequest;
use Illuminate\Support\Str;

class UserSettingsUpdateRequest extends DefaultApiRequest
{
    /**
     * @OA\Schema(
     *     schema="UserSettingsUpdateRequest",
     *     required={"share_type"},
     *     @OA\Property(
     *          property="share_type",
     *          type="integer"
     *     ),
     *     @OA\Property(
     *          property="shared_emails",
     *          type="array",
     *          @OA\Items(
     *              @OA\Property(
     *                  property="value",
     *                  type="string"
     *              )
     *          )
     *     )
     * )
     */

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
        ];
    }

    /**
     * Get custom messages for validator errors.
     *
     * @return array
     */
    public function messages()
    {
        return [
        ];
    }
}
