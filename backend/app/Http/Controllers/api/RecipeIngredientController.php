<?php

namespace App\Http\Controllers\api;

use App\Extensions\Responses\ApiResponses;
use App\Http\Controllers\Controller;
use App\Http\Requests\RecipeIngredientStoreRequest;
use App\Http\Resources\RecipeIngredientResource;
use App\Models\Recipe;
use App\Models\RecipeIngredient;
use App\Models\Unit;

class RecipeIngredientController extends Controller
{
    /**
     * @OA\Get(
     *      path="/api/recipes/{recipeId}/ingredient",
     *      security={{"BearerAuth":{}}},
     *      tags={"Recipe Ingredients"},
     *      summary="Get list of recipe ingredients",
     *      @OA\Parameter(
     *          parameter="recipeId",
     *          name="recipeId",
     *          in="path",
     *          required=true,
     *          @OA\Schema(
     *              type="integer"
     *          )
     *      ),
     *      @OA\Response(
     *          response="200",
     *          description="Success",
     *          @OA\JsonContent(
     *              @OA\Property(
     *                  property="data",
     *                  type="array",
     *                  @OA\Items(
     *                      ref="#/components/schemas/RecipeIngredient"
     *                  )
     *              )
     *          )
     *      ),
     *      @OA\Response(
     *          response="404",
     *          description="Not found",
     *          @OA\JsonContent(
     *              ref="#/components/schemas/NotFoundError"
     *          )
     *      )
     * )
     * @param $recipeId
     * @return \Illuminate\Http\JsonResponse|\Illuminate\Http\Resources\Json\AnonymousResourceCollection
     */
    public function index($recipeId)
    {
        if (!Recipe::findQuery()->where('id', $recipeId)->exists())
            return ApiResponses::objectNotFound(__('Przepis o podanym identyfikatorze nie istnieje'));

        return RecipeIngredientResource::collection(RecipeIngredient::query()->where('recipe_id', $recipeId)->get());
    }

    /**
     * @OA\Get(
     *      path="/api/recipes/{recipeId}/ingredient/{id}",
     *      security={{"BearerAuth":{}}},
     *      tags={"Recipe Ingredients"},
     *      summary="Get recipe ingredient by Id",
     *      @OA\Parameter(
     *          parameter="recipeId",
     *          name="recipeId",
     *          in="path",
     *          required=true,
     *          @OA\Schema(
     *              type="integer"
     *          )
     *      ),
     *      @OA\Parameter(
     *          parameter="id",
     *          name="id",
     *          in="path",
     *          required=true,
     *          @OA\Schema(
     *              type="integer"
     *          )
     *      ),
     *      @OA\Response(
     *          response="200",
     *          description="Success",
     *          @OA\JsonContent(
     *              ref="#/components/schemas/RecipeIngredient"
     *          )
     *      ),
     *      @OA\Response(
     *          response="404",
     *          description="Not found",
     *          @OA\JsonContent(
     *              ref="#/components/schemas/NotFoundError"
     *          )
     *      )
     * )
     * @param $recipeId
     * @return RecipeIngredientResource|\Illuminate\Http\JsonResponse
     */
    public function show($recipeId, $id)
    {
        if (!Recipe::findQuery()->where('id', $recipeId)->exists())
            return ApiResponses::objectNotFound(__('Przepis o podanym identyfikatorze nie istnieje'));

        $ingredient = RecipeIngredient::query()->where('id', $id)->where('recipe_id', $recipeId)->first();

        if (!$ingredient)
            return ApiResponses::objectNotFound(__('Składnik o podanym identyfikatorze nie istnieje'));

        return new RecipeIngredientResource($ingredient);
    }

    /**
     * @OA\Schema(
     *     schema="RecipeIngredientRequest",
     *     required={"ingredient_id", "value", "unit_id"},
     *     @OA\Property(
     *          property="ingredient_id",
     *          type="integer"
     *     ),
     *     @OA\Property(
     *          property="value",
     *          type="number"
     *     ),
     *     @OA\Property(
     *          property="unit_id",
     *          type="integer"
     *     )
     * )
     */

    /**
     * @OA\Post(
     *      path="/api/recipes/{recipeId}/ingredient",
     *      security={{"BearerAuth":{}}},
     *      tags={"Recipe Ingredients"},
     *      summary="Create new recipe ingredient",
     *      @OA\Parameter(
     *          parameter="recipeId",
     *          name="recipeId",
     *          in="path",
     *          required=true,
     *          @OA\Schema(
     *              type="integer"
     *          )
     *      ),
     *      @OA\RequestBody(
     *          required=true,
     *          @OA\JsonContent(
     *              ref="#/components/schemas/RecipeIngredientRequest"
     *          )
     *      ),
     *      @OA\Response(
     *          response="201",
     *          description="Success",
     *          @OA\JsonContent(
     *              ref="#/components/schemas/RecipeIngredient"
     *          )
     *      ),
     *      @OA\Response(
     *          response="403",
     *          description="Forbidden",
     *          @OA\JsonContent(
     *              ref="#/components/schemas/ForbiddenError"
     *          )
     *      ),
     *      @OA\Response(
     *          response="404",
     *          description="Not found",
     *          @OA\JsonContent(
     *              ref="#/components/schemas/NotFoundError"
     *          )
     *      ),
     *      @OA\Response(
     *          response="406",
     *          description="Validation errors",
     *          @OA\JsonContent(
     *              ref="#/components/schemas/ValidationErrors"
     *          )
     *      )
     * )
     * @param RecipeIngredientStoreRequest $request
     * @param $recipeId
     * @return RecipeIngredientResource|\Illuminate\Http\JsonResponse
     */
    public function store(RecipeIngredientStoreRequest $request, $recipeId)
    {
        if (!Recipe::query()->where('id', $recipeId)->exists())
            return ApiResponses::objectNotFound(__('Przepis o podanym identyfikatorze nie istnieje'));

        if (!Recipe::query()->where('id', $recipeId)->where('user_id', auth()->id())->exists())
            return ApiResponses::forbidden(__('Nie jesteś autorem przepisu'));

        $request->validated();

        $ingredient = new RecipeIngredient();
        $ingredient->fill([
            'recipe_id' => $recipeId,
            'ingredient_id' => $request->get('ingredient_id'),
            'value' => $request->get('value'),
            'unit_id' => $request->get('unit_id', Unit::query()->first()->id)
        ]);
        $ingredient->save();

        return new RecipeIngredientResource($ingredient);
    }

    /**
     * @OA\Put(
     *      path="/api/recipes/{recipeId}/ingredient/{id}",
     *      security={{"BearerAuth":{}}},
     *      tags={"Recipe Ingredients"},
     *      summary="Update recipe ingredient",
     *      @OA\Parameter(
     *          parameter="recipeId",
     *          name="recipeId",
     *          in="path",
     *          required=true,
     *          @OA\Schema(
     *              type="integer"
     *          )
     *      ),
     *      @OA\Parameter(
     *          parameter="id",
     *          name="id",
     *          in="path",
     *          required=true,
     *          @OA\Schema(
     *              type="integer"
     *          )
     *      ),
     *      @OA\RequestBody(
     *          required=true,
     *          @OA\JsonContent(
     *              ref="#/components/schemas/RecipeIngredientRequest"
     *          )
     *      ),
     *      @OA\Response(
     *          response="201",
     *          description="Success",
     *          @OA\JsonContent(
     *              ref="#/components/schemas/RecipeIngredient"
     *          )
     *      ),
     *      @OA\Response(
     *          response="403",
     *          description="Forbidden",
     *          @OA\JsonContent(
     *              ref="#/components/schemas/ForbiddenError"
     *          )
     *      ),
     *      @OA\Response(
     *          response="404",
     *          description="Not found",
     *          @OA\JsonContent(
     *              ref="#/components/schemas/NotFoundError"
     *          )
     *      ),
     *      @OA\Response(
     *          response="406",
     *          description="Validation errors",
     *          @OA\JsonContent(
     *              ref="#/components/schemas/ValidationErrors"
     *          )
     *      )
     * )
     * @param RecipeIngredientStoreRequest $request
     * @param $recipeId
     * @param $id
     * @return RecipeIngredientResource|\Illuminate\Http\JsonResponse
     */
    public function update(RecipeIngredientStoreRequest $request, $recipeId, $id)
    {
        if (!Recipe::query()->where('id', $recipeId)->exists())
            return ApiResponses::objectNotFound(__('Przepis o podanym identyfikatorze nie istnieje'));

        if (!Recipe::query()->where('id', $recipeId)->where('user_id', auth()->id())->exists())
            return ApiResponses::forbidden(__('Nie jesteś autorem przepisu'));

        $request->validated();

        $ingredient = RecipeIngredient::query()->where('id', $id)->where('recipe_id', $recipeId)->first();

        if (!$ingredient)
            return ApiResponses::objectNotFound(__('Składnik o podanym identyfikatorze nie istnieje'));

        $ingredient->fill([
            'recipe_id' => $recipeId,
            'ingredient_id' => $request->get('ingredient_id'),
            'value' => $request->get('value'),
            'unit_id' => $request->get('unit_id', Unit::query()->first()->id)
        ]);
        $ingredient->save();

        return new RecipeIngredientResource($ingredient);
    }

    /**
     * @OA\Delete(
     *      path="/api/recipes/{recipeId}/ingredient/{id}",
     *      security={{"BearerAuth":{}}},
     *      tags={"Recipe Ingredients"},
     *      summary="Delete recipe ingredient",
     *      @OA\Parameter(
     *          parameter="recipeId",
     *          name="recipeId",
     *          in="path",
     *          required=true,
     *          @OA\Schema(
     *              type="integer"
     *          )
     *      ),
     *      @OA\Parameter(
     *          parameter="id",
     *          name="id",
     *          in="path",
     *          required=true,
     *          @OA\Schema(
     *              type="integer"
     *          )
     *      ),
     *      @OA\Response(
     *          response="200",
     *          description="Success"
     *      ),
     *      @OA\Response(
     *          response="403",
     *          description="Forbidden",
     *          @OA\JsonContent(
     *              ref="#/components/schemas/ForbiddenError"
     *          )
     *      ),
     *      @OA\Response(
     *          response="404",
     *          description="Not found",
     *          @OA\JsonContent(
     *              ref="#/components/schemas/NotFoundError"
     *          )
     *      )
     * )
     * @param $recipeId
     * @param $id
     * @return \Illuminate\Http\JsonResponse
     * @throws \Exception
     */
    public function delete($recipeId, $id)
    {
        if (!Recipe::query()->where('id', $recipeId)->exists())
            return ApiResponses::objectNotFound(__('Przepis o podanym identyfikatorze nie istnieje'));

        if (!Recipe::query()->where('id', $recipeId)->where('user_id', auth()->id())->exists())
            return ApiResponses::forbidden(__('Nie jesteś autorem przepisu'));

        $ingredient = RecipeIngredient::query()->where('id', $id)->where('recipe_id', $recipeId)->first();

        if (!$ingredient)
            return ApiResponses::objectNotFound(__('Składnik o podanym identyfikatorze nie istnieje'));

        $ingredient->delete();

        return ApiResponses::success();
    }
}
