<?php

namespace App\Http\Controllers\api;

use App\Extensions\Responses\ApiResponses;
use App\Http\Controllers\Controller;
use App\Http\Requests\IngredientStoreRequest;
use App\Http\Resources\IngredientResource;
use App\Models\Ingredient;
use App\Models\Unit;

class IngredientsController extends Controller
{
    /**
     * @OA\Get(
     *      path="/api/ingredients",
     *      description="List",
     *      security={{"BearerAuth":{}}},
     *      tags={"Ingredients"},
     *      summary="Get ingredients list",
     *      @OA\Parameter(
     *          parameter="query",
     *          name="query",
     *          in="path",
     *          @OA\Schema(
     *              type="string"
     *          )
     *      ),
     *      @OA\Parameter(
     *          parameter="excludedIds",
     *          name="excludedIds",
     *          in="query",
     *          @OA\Schema(
     *              type="array",
     *              @OA\Items(
     *                  type="integer"
     *              )
     *          )
     *      ),
     *      @OA\Parameter(
     *          parameter="page",
     *          name="page",
     *          in="query",
     *          @OA\Schema(
     *              type="integer",
     *              default=1
     *          )
     *      ),
     *      @OA\Parameter(
     *          parameter="perPage",
     *          name="perPage",
     *          in="query",
     *          @OA\Schema(
     *              type="integer",
     *              default=10
     *          )
     *      ),
     *      @OA\Response(
     *          response="200",
     *          description="Success",
     *          @OA\JsonContent(
     *              @OA\Property(
     *                  property="data",
     *                  type="array",
     *                  @OA\Items(
     *                      ref="#/components/schemas/Ingredient"
     *                  )
     *              ),
     *              @OA\Property(
     *                  property="links",
     *                  type="object",
 *                      ref="#/components/schemas/PagerLinks"
     *              ),
     *              @OA\Property(
     *                  property="meta",
     *                  type="object",
 *                      ref="#/components/schemas/PagerMeta"
     *              )
     *          )
     *      )
     * )
     */
    public function index()
    {
        $query = strip_tags(request()->get('query'));
        $query = mb_strtolower($query);

        $perPage = request()->get('perPage', 10);
        $page = request()->get('page', 1);

        $excludedIds = request()->get('excludedIds', []);

        return IngredientResource::collection(Ingredient::query()
            ->whereNotIn('id', $excludedIds)
            ->whereRaw('LOWER(name) LIKE "%'.$query.'%"')
            ->where('user_id', auth()->id())
            ->paginate($perPage, ['*'], 'page', $page)
        );
    }

    /**
     * @OA\Get(
     *      path="/api/ingredients/{id}",
     *      security={{"BearerAuth":{}}},
     *      tags={"Ingredients"},
     *      summary="Get ingredient by id",
     *      @OA\Parameter(
     *          parameter="id",
     *          name="id",
     *          in="path",
     *          required=true,
     *          @OA\Schema(
     *              type="integer"
     *          )
     *      ),
     *      @OA\Response(
     *          response="200",
     *          description="Success",
     *          @OA\JsonContent(
     *              ref="#/components/schemas/Ingredient"
     *          )
     *      ),
     *      @OA\Response(
     *          response="404",
     *          description="Ingredient not found",
     *          @OA\JsonContent(
     *              ref="#/components/schemas/NotFoundError"
     *          )
     *      )
     * )
     *
     * @param $id
     * @return IngredientResource|\Illuminate\Http\JsonResponse
     */
    public function show($id)
    {
        $ingredient = Ingredient::query()->where('user_id', auth()->id())->find($id);

        if (!$ingredient)
            return ApiResponses::objectNotFound(__('Składnik o podanym identyfikatorze nie istnieje'));

        return new IngredientResource($ingredient);
    }

    /**
     * @OA\Schema(
     *     schema="IngredientRequest",
     *     required={"name"},
     *     @OA\Property(
     *          property="name",
     *          type="string"
     *     )
     * )
     */

    /**
     * @OA\Post(
     *      path="/api/ingredients",
     *      security={{"BearerAuth":{}}},
     *      tags={"Ingredients"},
     *      summary="Create new ingredient",
     *      @OA\RequestBody(
     *          required=true,
     *          @OA\JsonContent(
     *              ref="#/components/schemas/IngredientRequest"
     *          )
     *      ),
     *      @OA\Response(
     *          response="201",
     *          description="Success",
     *          @OA\JsonContent(
     *              ref="#/components/schemas/Ingredient"
     *          )
     *      )
     * )
     * @param IngredientStoreRequest $request
     * @return IngredientResource
     */
    public function store(IngredientStoreRequest $request)
    {
        $name = $request->input('name');
        $name = htmlentities($name);

        $ingredient = new Ingredient();
        $ingredient->name = $name;
        $ingredient->user_id = auth()->id();
        $ingredient->save();

        return new IngredientResource($ingredient);
    }

    /**
     * @OA\Put(
     *      path="/api/ingredients/{id}",
     *      security={{"BearerAuth":{}}},
     *      tags={"Ingredients"},
     *      summary="Update ingredient",
     *      @OA\Parameter(
     *          parameter="id",
     *          name="id",
     *          in="path",
     *          required=true,
     *          @OA\Schema(
     *              type="integer"
     *          )
     *      ),
     *      @OA\RequestBody(
     *          required=true,
     *          @OA\JsonContent(
     *              ref="#/components/schemas/IngredientRequest"
     *          )
     *      ),
     *      @OA\Response(
     *          response="201",
     *          description="Success",
     *          @OA\JsonContent(
     *              ref="#/components/schemas/Ingredient"
     *          )
     *      )
     * )
     * @param IngredientStoreRequest $request
     * @param $id
     * @return IngredientResource|\Illuminate\Http\JsonResponse
     */
    public function update(IngredientStoreRequest $request, $id)
    {
        $ingredient = Ingredient::query()->where('user_id', auth()->id())->find($id);

        if (!$ingredient)
            return ApiResponses::objectNotFound(__('Składnik o podanym identyfikatorze nie istnieje'));

        $request->validated();

        $name = $request->input('name');
        $name = htmlentities($name);

        $ingredient->name = $name;
        $ingredient->save();

        return new IngredientResource($ingredient);
    }

    /**
     * @OA\Delete(
     *      path="/api/ingredients/{id}",
     *      security={{"BearerAuth":{}}},
     *      tags={"Ingredients"},
     *      summary="Delete ingredient",
     *      @OA\Parameter(
     *          parameter="id",
     *          name="id",
     *          in="path",
     *          required=true,
     *          @OA\Schema(
     *              type="integer"
     *          )
     *      ),
     *      @OA\Response(
     *          response="200",
     *          description="Success"
     *      ),
     *      @OA\Response(
     *          response="404",
     *          description="Not found",
     *          @OA\JsonContent(
     *              ref="#/components/schemas/NotFoundError"
     *          )
     *      )
     * )
     * @param $id
     * @return \Illuminate\Http\JsonResponse
     * @throws \Exception
     */
    public function delete($id)
    {
        $ingredient = Ingredient::query()->where('user_id', auth()->id())->where('id', $id)->first();

        if (!$ingredient)
            return ApiResponses::objectNotFound(__('Składnik o podanym identyfikatorze nie istnieje'));

        $ingredient->delete();

        return ApiResponses::success();
    }
}
