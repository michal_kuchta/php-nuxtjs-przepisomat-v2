<?php

namespace App\Http\Controllers\api;

use App\Extensions\Responses\ApiResponses;
use App\Http\Controllers\Controller;
use App\Http\Requests\CategoryStoreRequest;
use App\Http\Requests\RecipeStoreRequest;
use App\Http\Resources\CategoryResource;
use App\Http\Resources\CategoryTreeResource;
use App\Http\Resources\RecipeRateResource;
use App\Http\Resources\RecipeResource;
use App\Models\Category;
use App\Models\Recipe;
use App\Models\RecipeIngredient;
use App\Models\RecipePhoto;
use App\Models\RecipeRate;
use App\Models\RecipeShare;
use App\Models\RecipeStep;
use App\Models\UserFavoriteRecipe;
use Illuminate\Support\Str;

class CategoriesController extends Controller
{
    /**
     * @OA\Get(
     *      path="/api/categories",
     *      security={{"BearerAuth":{}}},
     *      tags={"Categories"},
     *      summary="Get categories list",
     *      @OA\Parameter(
     *          parameter="page",
     *          name="page",
     *          in="query",
     *          @OA\Schema(
     *              type="integer",
     *              default=1
     *          )
     *      ),
     *      @OA\Parameter(
     *          parameter="perPage",
     *          name="perPage",
     *          in="query",
     *          @OA\Schema(
     *              type="integer",
     *              default=10
     *          )
     *      ),
     *      @OA\Parameter(
     *          parameter="orderBy",
     *          name="orderBy",
     *          in="query",
     *          @OA\Schema(
     *              type="string",
     *              default="date"
     *          )
     *      ),
     *      @OA\Parameter(
     *          parameter="orderDirection",
     *          name="orderDirection",
     *          in="query",
     *          @OA\Schema(
     *              type="string",
     *              default="desc"
     *          )
     *      ),
     *      @OA\Parameter(
     *          parameter="query",
     *          name="query",
     *          in="query",
     *          @OA\Schema(
     *              type="string",
     *              default=""
     *          )
     *      ),
     *      @OA\Response(
     *          response="200",
     *          description="Success",
     *          @OA\JsonContent(
     *              @OA\Property(
     *                  property="data",
     *                  type="array",
     *                  @OA\Items(
     *                      ref="#/components/schemas/Category"
     *                  )
     *              ),
     *              @OA\Property(
     *                  property="links",
     *                  type="object",
     *                      ref="#/components/schemas/PagerLinks"
     *              ),
     *              @OA\Property(
     *                  property="meta",
     *                  type="object",
     *                      ref="#/components/schemas/PagerMeta"
     *              )
     *          )
     *      )
     * )
     */
    public function index()
    {
        $perPage = request()->get('perPage', 10);
        $page = request()->get('page', 1);
        $query = trim(request()->get('query', ''));

        $orderBy = request()->get('orderBy', 'date');
        if (!in_array($orderBy, ['date', 'name']))
            $orderBy = 'date';

        switch ($orderBy)
        {
            case "date":
                $orderBy = 'created_at';
        }

        $orderDirection = request()->get('orderDirection', 'desc');
        if (!in_array($orderDirection, ['asc', 'desc']))
            $orderDirection = 'desc';

        return CategoryResource::collection(Category::findQuery($query)
            ->orderBy($orderBy, $orderDirection)
            ->paginate($perPage, ['*'], 'page', $page));
    }

    /**
     * @OA\Get(
     *      path="/api/categories/tree",
     *      security={{"BearerAuth":{}}},
     *      tags={"Categories"},
     *      summary="Get categories list as tree hierarchy",
     *      @OA\Parameter(
     *          parameter="level",
     *          name="level",
     *          in="query",
     *          @OA\Schema(
     *              type="number",
     *              default=""
     *          )
     *      ),
     *      @OA\Response(
     *          response="200",
     *          description="Success",
     *          @OA\JsonContent(
     *              @OA\Property(
     *                  property="data",
     *                  type="array",
     *                  @OA\Items(
     *                      ref="#/components/schemas/Category"
     *                  )
     *              ),
     *              @OA\Property(
     *                  property="links",
     *                  type="object",
     *                      ref="#/components/schemas/PagerLinks"
     *              ),
     *              @OA\Property(
     *                  property="meta",
     *                  type="object",
     *                      ref="#/components/schemas/PagerMeta"
     *              )
     *          )
     *      )
     * )
     */
    public function tree()
    {
        $level = intval(request()->get('level', 1));
        $level = $level < 1 ? 1 : $level;

        return CategoryTreeResource::collection(Category::findQuery()
            ->where('level', $level)
            ->get());
    }

    /**
     * @OA\Get(
     *      path="/api/categories/{id}",
     *      security={{"BearerAuth":{}}},
     *      tags={"Categories"},
     *      summary="Get category by id",
     *      @OA\Parameter(
     *          parameter="id",
     *          name="id",
     *          in="path",
     *          required=true,
     *          @OA\Schema(
     *              type="integer"
     *          )
     *      ),
     *      @OA\Response(
     *          response="200",
     *          description="Success",
     *          @OA\JsonContent(
     *              ref="#/components/schemas/Category"
     *          )
     *      ),
     *      @OA\Response(
     *          response="404",
     *          description="Ingredient not found",
     *          @OA\JsonContent(
     *              ref="#/components/schemas/NotFoundError"
     *          )
     *      )
     * )
     *
     * @param $id
     * @return RecipeResource|\Illuminate\Http\JsonResponse
     */
    public function show($id)
    {
        $model = Category::findQuery()->find($id);

        if (!$model)
            return ApiResponses::objectNotFound(__('Kategoria o podanym identyfikatorze nie istnieje'));

        return new CategoryResource($model);
    }

    /**
     * @OA\Post(
     *      path="/api/categories",
     *      security={{"BearerAuth":{}}},
     *      tags={"Categories"},
     *      summary="Create new category",
     *      @OA\RequestBody(
     *          required=true,
     *          @OA\JsonContent(
     *              ref="#/components/schemas/CategoryRequest"
     *          )
     *      ),
     *      @OA\Response(
     *          response="201",
     *          description="Success",
     *          @OA\JsonContent(
     *              ref="#/components/schemas/Category"
     *          )
     *      ),
     *      @OA\Response(
     *          response="406",
     *          description="Success",
     *          @OA\JsonContent(
     *              ref="#/components/schemas/ValidationErrors"
     *          )
     *      )
     * )
     * @param CategoryStoreRequest $request
     * @return CategoryResource
     */
    public function store(CategoryStoreRequest $request)
    {
        $request->replace(array_merge($request->all(), ['slug' => Str::slug($request->input('name'))]));
        $request->validated();

        $model = new Category();

        $parent = Category::query()->where('id', intval($request->get('parent_id')))->first();

        $model->name = $request->get('name');
        $model->parent_id = intval($request->get('parent_id'));
        $model->slug = Str::slug($request->get('name'));
        $model->level = $parent ? $parent->level + 1 : 1;
        $model->position = 0;
        $model->is_active = $request->get('is_active', true);
        $model->has_children = false;
        $model->save();

        $model->ancestors = ($model->parent_id > 0 ? $parent->ancestors : '#') . $model->id . '#';
        $model->save();

        return new CategoryResource($model);
    }

    /**
     * @OA\Put(
     *      path="/api/categories/{id}",
     *      security={{"BearerAuth":{}}},
     *      tags={"Categories"},
     *      summary="Update category",
     *      @OA\Parameter(
     *          parameter="id",
     *          name="id",
     *          in="path",
     *          required=true,
     *          @OA\Schema(
     *              type="integer"
     *          )
     *      ),
     *      @OA\RequestBody(
     *          required=true,
     *          @OA\JsonContent(
     *              ref="#/components/schemas/CategoryRequest"
     *          )
     *      ),
     *      @OA\Response(
     *          response="201",
     *          description="Success",
     *          @OA\JsonContent(
     *              ref="#/components/schemas/Category"
     *          )
     *      ),
     *      @OA\Response(
     *          response="406",
     *          description="Success",
     *          @OA\JsonContent(
     *              ref="#/components/schemas/ValidationErrors"
     *          )
     *      )
     * )
     * @param CategoryStoreRequest $request
     * @param $id
     * @return CategoryResource|\Illuminate\Http\JsonResponse
     */
    public function update(CategoryStoreRequest $request, $id)
    {
        $model = Category::query()->find($id);

        if (!$model)
            return ApiResponses::objectNotFound(__('Kategoria o podanym identyfikatorze nie istnieje'));

        $request->replace(array_merge($request->all(), ['slug' => Str::slug($request->input('name'))]));
        $request->validated();

        $parent = Category::query()->where('id', intval($request->get('parent_id')))->first();

        $model->name = $request->get('name');
        $model->parent_id = intval($request->get('parent_id'));
        $model->slug = Str::slug($request->get('name'));
        $model->level = $parent ? $parent->level + 1 : 1;
        $model->position = 0;
        $model->is_active = $request->get('is_active', true);
        $model->has_children = false;
        $model->ancestors = '';
        $model->save();

        $model->ancestors = ($model->parent_id > 0 ? $parent->ancestors : '#') . $model->id . '#';
        $model->save();

        return new CategoryResource($model);
    }

    /**
     * @OA\Delete(
     *      path="/api/categories/{id}",
     *      security={{"BearerAuth":{}}},
     *      tags={"Categories"},
     *      summary="Delete category",
     *      @OA\Parameter(
     *          parameter="id",
     *          name="id",
     *          in="path",
     *          required=true,
     *          @OA\Schema(
     *              type="integer"
     *          )
     *      ),
     *      @OA\Response(
     *          response="200",
     *          description="Success"
     *      ),
     *      @OA\Response(
     *          response="404",
     *          description="Not found",
     *          @OA\JsonContent(
     *              ref="#/components/schemas/NotFoundError"
     *          )
     *      )
     * )
     * @param $id
     * @return \Illuminate\Http\JsonResponse
     * @throws \Exception
     */
    public function delete($id)
    {
        $model = Category::query()->where('id', $id)->first();

        if (!$model)
            return ApiResponses::objectNotFound(__('Kategoria o podanym identyfikatorze nie istnieje'));

        Category::query()->where('ancestors', 'LIKE', "%#$id#%")->delete();

        return ApiResponses::success();
    }
}
