<?php

namespace App\Http\Controllers\api;

use App\Extensions\Responses\ApiResponses;
use App\Http\Controllers\Controller;
use App\Http\Requests\UserPasswordUpdateRequest;
use App\Http\Requests\UserSettingsUpdateRequest;
use App\Http\Requests\UserUpdateRequest;
use App\Http\Resources\UserResource;
use App\Models\Recipe;
use App\Models\RecipeShare;
use App\Models\UserShare;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Str;

class HelpersController extends Controller
{
    /**
     * @OA\Put(
     *      path="/api/helpers/slug",
     *      security={{"BearerAuth":{}}},
     *      tags={"Helpers"},
     *      summary="Return slug made from input",
     *      @OA\RequestBody(
     *          required=true,
     *          @OA\JsonContent(
     *              @OA\Property(
     *                  property="text",
     *                  type="string"
     *              )
     *          )
     *      ),
     *      @OA\Response(
     *          response="200",
     *          description="Success",
     *          @OA\JsonContent(
     *              @OA\Property(
     *                  property="text",
     *                  type="string"
     *              )
     *          )
     *      ),
     *      @OA\Response(
     *          response="406",
     *          description="Validation errors",
     *          @OA\JsonContent(
     *              ref="#/components/schemas/ValidationErrors"
     *          )
     *      )
     * )
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function slug(Request $request)
    {
        if (!request()->has('text') || empty(request()->get('text', '')))
            return ApiResponses::validationErrors(__('Niepoprawy tekst'));

        return response()->json([
            'text' => Str::slug(request()->get('text', ''))
        ]);
    }
}
