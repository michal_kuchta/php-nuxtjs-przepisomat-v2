<?php

namespace App\Http\Controllers\api;

use App\Extensions\Responses\ApiResponses;
use App\Http\Controllers\Controller;
use App\Http\Requests\RecipeStepStoreRequest;
use App\Http\Resources\RecipeStepResource;
use App\Models\Recipe;
use App\Models\RecipeStep;

class RecipeStepController extends Controller
{
    /**
     * @OA\Get(
     *      path="/api/recipes/{recipeId}/step",
     *      security={{"BearerAuth":{}}},
     *      tags={"Recipe Steps"},
     *      summary="Get list of recipe steps",
     *      @OA\Parameter(
     *          parameter="recipeId",
     *          name="recipeId",
     *          in="path",
     *          required=true,
     *          @OA\Schema(
     *              type="integer"
     *          )
     *      ),
     *      @OA\Response(
     *          response="200",
     *          description="Success",
     *          @OA\JsonContent(
     *              @OA\Property(
     *                  property="data",
     *                  type="array",
     *                  @OA\Items(
     *                      ref="#/components/schemas/RecipeStep"
     *                  )
     *              )
     *          )
     *      ),
     *      @OA\Response(
     *          response="404",
     *          description="Not found",
     *          @OA\JsonContent(
     *              ref="#/components/schemas/NotFoundError"
     *          )
     *      )
     * )
     * @param $recipeId
     * @return \Illuminate\Http\JsonResponse|\Illuminate\Http\Resources\Json\AnonymousResourceCollection
     */
    public function index($recipeId)
    {
        if (!Recipe::findQuery()->where('id', $recipeId)->exists())
            return ApiResponses::objectNotFound(__('Przepis o podanym identyfikatorze nie istnieje'));

        return RecipeStepResource::collection(RecipeStep::query()->where('recipe_id', $recipeId)->get());
    }

    /**
     * @OA\Get(
     *      path="/api/recipes/{recipeId}/step/{id}",
     *      security={{"BearerAuth":{}}},
     *      tags={"Recipe Steps"},
     *      summary="Get recipe step by Id",
     *      @OA\Parameter(
     *          parameter="recipeId",
     *          name="recipeId",
     *          in="path",
     *          required=true,
     *          @OA\Schema(
     *              type="integer"
     *          )
     *      ),
     *      @OA\Parameter(
     *          parameter="id",
     *          name="id",
     *          in="path",
     *          required=true,
     *          @OA\Schema(
     *              type="integer"
     *          )
     *      ),
     *      @OA\Response(
     *          response="200",
     *          description="Success",
     *          @OA\JsonContent(
     *              ref="#/components/schemas/RecipeStep"
     *          )
     *      ),
     *      @OA\Response(
     *          response="403",
     *          description="Forbidden",
     *          @OA\JsonContent(
     *              ref="#/components/schemas/ForbiddenError"
     *          )
     *      ),
     *      @OA\Response(
     *          response="404",
     *          description="Not found",
     *          @OA\JsonContent(
     *              ref="#/components/schemas/NotFoundError"
     *          )
     *      )
     * )
     * @param $recipeId
     * @return RecipeStepResource|\Illuminate\Http\JsonResponse
     */
    public function show($recipeId, $id)
    {
        if (!Recipe::findQuery()->where('id', $recipeId)->exists())
            return ApiResponses::objectNotFound(__('Przepis o podanym identyfikatorze nie istnieje'));

        $step = RecipeStep::query()->where('id', $id)->where('recipe_id', $recipeId)->first();

        if (!$step)
            return ApiResponses::objectNotFound(__('Krok o podanym identyfikatorze nie istnieje'));

        return new RecipeStepResource($step);
    }

    /**
     * @OA\Schema(
     *     schema="RecipeStepRequest",
     *     required={"position"},
     *     @OA\Property(
     *          property="description",
     *          type="string"
     *     ),
     *     @OA\Property(
     *          property="photo_id",
     *          type="integer"
     *     ),
     *     @OA\Property(
     *          property="position",
     *          type="integer"
     *     )
     * )
     */

    /**
     * @OA\Post(
     *      path="/api/recipes/{recipeId}/step",
     *      security={{"BearerAuth":{}}},
     *      tags={"Recipe Steps"},
     *      summary="Create new recipe step",
     *      @OA\Parameter(
     *          parameter="recipeId",
     *          name="recipeId",
     *          in="path",
     *          required=true,
     *          @OA\Schema(
     *              type="integer"
     *          )
     *      ),
     *      @OA\RequestBody(
     *          required=true,
     *          @OA\JsonContent(
     *              ref="#/components/schemas/RecipeStepRequest"
     *          )
     *      ),
     *      @OA\Response(
     *          response="201",
     *          description="Success",
     *          @OA\JsonContent(
     *              ref="#/components/schemas/RecipeStep"
     *          )
     *      ),
     *      @OA\Response(
     *          response="403",
     *          description="Forbidden",
     *          @OA\JsonContent(
     *              ref="#/components/schemas/ForbiddenError"
     *          )
     *      ),
     *      @OA\Response(
     *          response="404",
     *          description="Not found",
     *          @OA\JsonContent(
     *              ref="#/components/schemas/NotFoundError"
     *          )
     *      ),
     *      @OA\Response(
     *          response="406",
     *          description="Validation errors",
     *          @OA\JsonContent(
     *              ref="#/components/schemas/ValidationErrors"
     *          )
     *      )
     * )
     * @param RecipeStepStoreRequest $request
     * @param $recipeId
     * @return RecipeStepResource|\Illuminate\Http\JsonResponse
     */
    public function store(RecipeStepStoreRequest $request, $recipeId)
    {
        if (!Recipe::query()->where('id', $recipeId)->exists())
            return ApiResponses::objectNotFound(__('Przepis o podanym identyfikatorze nie istnieje'));

        if (!Recipe::query()->where('id', $recipeId)->where('user_id', auth()->id())->exists())
            return ApiResponses::forbidden(__('Nie jesteś autorem przepisu'));

        $request->validated();

        $step = new RecipeStep();
        $step->fill([
            'recipe_id' => $recipeId,
            'description' => $request->get('description'),
            'photo_id' => $request->get('photo_id'),
            'position' => $request->get('position')
        ]);
        $step->save();

        return new RecipeStepResource($step);
    }

    /**
     * @OA\Put(
     *      path="/api/recipes/{recipeId}/step/{id}",
     *      security={{"BearerAuth":{}}},
     *      tags={"Recipe Steps"},
     *      summary="Update recipe step",
     *      @OA\Parameter(
     *          parameter="recipeId",
     *          name="recipeId",
     *          in="path",
     *          required=true,
     *          @OA\Schema(
     *              type="integer"
     *          )
     *      ),
     *      @OA\Parameter(
     *          parameter="id",
     *          name="id",
     *          in="path",
     *          required=true,
     *          @OA\Schema(
     *              type="integer"
     *          )
     *      ),
     *      @OA\RequestBody(
     *          required=true,
     *          @OA\JsonContent(
     *              ref="#/components/schemas/RecipeStepRequest"
     *          )
     *      ),
     *      @OA\Response(
     *          response="201",
     *          description="Success",
     *          @OA\JsonContent(
     *              ref="#/components/schemas/RecipeStep"
     *          )
     *      ),
     *      @OA\Response(
     *          response="403",
     *          description="Forbidden",
     *          @OA\JsonContent(
     *              ref="#/components/schemas/ForbiddenError"
     *          )
     *      ),
     *      @OA\Response(
     *          response="404",
     *          description="Not found",
     *          @OA\JsonContent(
     *              ref="#/components/schemas/NotFoundError"
     *          )
     *      ),
     *      @OA\Response(
     *          response="406",
     *          description="Validation errors",
     *          @OA\JsonContent(
     *              ref="#/components/schemas/ValidationErrors"
     *          )
     *      )
     * )
     * @param RecipeStepStoreRequest $request
     * @param $recipeId
     * @param $id
     * @return RecipeStepResource|\Illuminate\Http\JsonResponse
     */
    public function update(RecipeStepStoreRequest $request, $recipeId, $id)
    {
        if (!Recipe::query()->where('id', $recipeId)->exists())
            return ApiResponses::objectNotFound(__('Przepis o podanym identyfikatorze nie istnieje'));

        if (!Recipe::query()->where('id', $recipeId)->where('user_id', auth()->id())->exists())
            return ApiResponses::forbidden(__('Nie jesteś autorem przepisu'));

        $request->validated();

        $step = RecipeStep::query()->where('id', $id)->where('recipe_id', $recipeId)->first();

        if (!$step)
            return ApiResponses::objectNotFound(__('Krok o podanym identyfikatorze nie istnieje'));

        $step->fill([
            'recipe_id' => $recipeId,
            'description' => $request->get('description'),
            'photo_id' => $request->get('photo_id'),
            'position' => $request->get('position')
        ]);
        $step->save();

        return new RecipeStepResource($step);
    }

    /**
     * @OA\Delete(
     *      path="/api/recipes/{recipeId}/step/{id}",
     *      security={{"BearerAuth":{}}},
     *      tags={"Recipe Steps"},
     *      summary="Delete recipe step",
     *      @OA\Parameter(
     *          parameter="recipeId",
     *          name="recipeId",
     *          in="path",
     *          required=true,
     *          @OA\Schema(
     *              type="integer"
     *          )
     *      ),
     *      @OA\Parameter(
     *          parameter="id",
     *          name="id",
     *          in="path",
     *          required=true,
     *          @OA\Schema(
     *              type="integer"
     *          )
     *      ),
     *      @OA\Response(
     *          response="200",
     *          description="Success"
     *      ),
     *      @OA\Response(
     *          response="403",
     *          description="Forbidden",
     *          @OA\JsonContent(
     *              ref="#/components/schemas/ForbiddenError"
     *          )
     *      ),
     *      @OA\Response(
     *          response="404",
     *          description="Not found",
     *          @OA\JsonContent(
     *              ref="#/components/schemas/NotFoundError"
     *          )
     *      )
     * )
     * @param $recipeId
     * @param $id
     * @return \Illuminate\Http\JsonResponse
     * @throws \Exception
     */
    public function delete($recipeId, $id)
    {
        if (!Recipe::query()->where('id', $recipeId)->exists())
            return ApiResponses::objectNotFound(__('Przepis o podanym identyfikatorze nie istnieje'));

        if (!Recipe::query()->where('id', $recipeId)->where('user_id', auth()->id())->exists())
            return ApiResponses::forbidden(__('Nie jesteś autorem przepisu'));

        $step = RecipeStep::query()->where('id', $id)->where('recipe_id', $recipeId)->first();

        if (!$step)
            return ApiResponses::objectNotFound(__('Krok o podanym identyfikatorze nie istnieje'));

        $step->delete();

        $steps = RecipeStep::query()->where('recipe_id', $recipeId)->orderBy('position', 'asc')->get();

        //Rearrange positions
        $position = 1;
        foreach ($steps as $step)
        {
            $step->position = $position++;
            $step->save();
        }

        return ApiResponses::success();
    }
}
