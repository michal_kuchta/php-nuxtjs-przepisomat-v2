<?php

namespace App\Http\Controllers\api;

use App\Extensions\Responses\ApiResponses;
use App\Http\Controllers\Controller;
use App\Http\Requests\UnitStoreRequest;
use App\Http\Resources\UnitResource;
use App\Models\Unit;

class UnitsController extends Controller
{
    /**
     * @OA\Get(
     *      path="/api/units",
     *      description="List",
     *      security={{"BearerAuth":{}}},
     *      tags={"Units"},
     *      summary="Get units list",
     *      @OA\Response(
     *          response="200",
     *          description="Success",
     *          @OA\JsonContent(
     *              @OA\Property(
     *                  property="data",
 *                      type="array",
     *                  @OA\Items(
     *                      ref="#/components/schemas/Unit"
     *                  )
     *              )
     *          )
     *      )
     * )
     */
    public function index()
    {
        return UnitResource::collection(Unit::get());
    }


    /**
     * @OA\Get(
     *      path="/api/units/{id}",
     *      security={{"BearerAuth":{}}},
     *      tags={"Units"},
     *      summary="Get unit by Id",
     *      @OA\Parameter(
     *          parameter="id",
     *          name="id",
     *          in="path",
     *          required=true,
     *          @OA\Schema(
     *              type="integer"
     *          )
     *      ),
     *      @OA\Response(
     *          response="200",
     *          description="Success",
     *          @OA\JsonContent(
     *              ref="#/components/schemas/Unit"
     *          )
     *      ),
     *      @OA\Response(
     *          response="404",
     *          description="Not found",
     *          @OA\JsonContent(
     *              ref="#/components/schemas/NotFoundError"
     *          )
     *      )
     * )
     * @param $id
     * @return UnitResource|\Illuminate\Http\JsonResponse
     */
    public function show($id)
    {
        if (!Unit::query()->where('id', $id)->exists())
            return ApiResponses::objectNotFound(__('Jednostka o podanym identyfikatorze nie istnieje'));

        return new UnitResource(Unit::query()->where('id', $id)->first());
    }

    /**
     * @OA\Schema(
     *     schema="UnitRequest",
     *     required={"name", "multiplicity", "abbr_quantity_one", "abbr_quantity_float", "abbr_quantity_two_four", "abbr_quantity_five_and_more"},
     *     @OA\Property(
     *          property="name",
     *          type="string"
     *     ),
     *     @OA\Property(
     *          property="child",
     *          type="number"
     *     ),
     *     @OA\Property(
     *          property="parent_id",
     *          type="integer"
     *     ),
     *     @OA\Property(
     *          property="multiplicity",
     *          type="number",
     *          format="double"
     *     ),
     *     @OA\Property(
     *          property="abbr_quantity_one",
     *          type="string"
     *     ),
     *     @OA\Property(
     *          property="abbr_quantity_float",
     *          type="string"
     *     ),
     *     @OA\Property(
     *          property="abbr_quantity_two_four",
     *          type="string"
     *     ),
     *     @OA\Property(
     *          property="abbr_quantity_five_and_more",
     *          type="string"
     *     )
     * )
     */

    /**
     * @OA\Post(
     *      path="/api/units",
     *      security={{"BearerAuth":{}}},
     *      tags={"Units"},
     *      summary="Create new unit",
     *      @OA\RequestBody(
     *          required=true,
     *          @OA\JsonContent(
     *              ref="#/components/schemas/UnitRequest"
     *          )
     *      ),
     *      @OA\Response(
     *          response="201",
     *          description="Success",
     *          @OA\JsonContent(
     *              ref="#/components/schemas/Unit"
     *          )
     *      ),
     *      @OA\Response(
     *          response="406",
     *          description="Validation errors",
     *          @OA\JsonContent(
     *              ref="#/components/schemas/ValidationErrors"
     *          )
     *      )
     * )
     * @param UnitStoreRequest $request
     * @return UnitResource|\Illuminate\Http\JsonResponse
     */
    public function store(UnitStoreRequest $request)
    {
        $request->validated();

        $unit = new Unit();
        $unit->fill([
            'name' => $request->get('name'),
            'child' => $request->get('child', 0),
            'parent_id' => $request->get('parent_id', 0),
            'multiplicity' => $request->get('multiplicity', 0),
            'abbr_quantity_one' => $request->get('abbr_quantity_one'),
            'abbr_quantity_float' => $request->get('abbr_quantity_float'),
            'abbr_quantity_two_four' => $request->get('abbr_quantity_two_four'),
            'abbr_quantity_five_and_more' => $request->get('abbr_quantity_five_and_more'),
        ]);
        $unit->save();

        return new UnitResource($unit);
    }

    /**
     * @OA\Put(
     *      path="/api/units/{id}",
     *      security={{"BearerAuth":{}}},
     *      tags={"Units"},
     *      summary="Update unit",
     *      @OA\Parameter(
     *          parameter="id",
     *          name="id",
     *          in="path",
     *          required=true,
     *          @OA\Schema(
     *              type="integer"
     *          )
     *      ),
     *      @OA\RequestBody(
     *          required=true,
     *          @OA\JsonContent(
     *              ref="#/components/schemas/UnitRequest"
     *          )
     *      ),
     *      @OA\Response(
     *          response="201",
     *          description="Success",
     *          @OA\JsonContent(
     *              ref="#/components/schemas/Unit"
     *          )
     *      ),
     *      @OA\Response(
     *          response="404",
     *          description="Not found",
     *          @OA\JsonContent(
     *              ref="#/components/schemas/NotFoundError"
     *          )
     *      ),
     *      @OA\Response(
     *          response="406",
     *          description="Validation errors",
     *          @OA\JsonContent(
     *              ref="#/components/schemas/ValidationErrors"
     *          )
     *      )
     * )
     * @param UnitStoreRequest $request
     * @param $id
     * @return UnitResource|\Illuminate\Http\JsonResponse
     */
    public function update(UnitStoreRequest $request, $id)
    {
        $request->validated();

        $unit = Unit::query()->where('id', $id)->first();

        if (!$unit)
            return ApiResponses::objectNotFound(__('Jednostka o podanym identyfikatorze nie istnieje'));

        $unit->fill([
            'name' => $request->get('name'),
            'child' => $request->get('child', 0),
            'parent_id' => $request->get('parent_id', 0),
            'multiplicity' => $request->get('multiplicity', 0),
            'abbr_quantity_one' => $request->get('abbr_quantity_one'),
            'abbr_quantity_float' => $request->get('abbr_quantity_float'),
            'abbr_quantity_two_four' => $request->get('abbr_quantity_two_four'),
            'abbr_quantity_five_and_more' => $request->get('abbr_quantity_five_and_more'),
        ]);
        $unit->save();

        return new UnitResource($unit);
    }

    /**
     * @OA\Delete(
     *      path="/api/units/{id}",
     *      security={{"BearerAuth":{}}},
     *      tags={"Units"},
     *      summary="Delete unit",
     *      @OA\Parameter(
     *          parameter="id",
     *          name="id",
     *          in="path",
     *          required=true,
     *          @OA\Schema(
     *              type="integer"
     *          )
     *      ),
     *      @OA\Response(
     *          response="200",
     *          description="Success"
     *      ),
     *      @OA\Response(
     *          response="404",
     *          description="Not found",
     *          @OA\JsonContent(
     *              ref="#/components/schemas/NotFoundError"
     *          )
     *      )
     * )
     * @param $id
     * @return \Illuminate\Http\JsonResponse
     * @throws \Exception
     */
    public function delete($id)
    {
        $unit = Unit::query()->where('id', $id)->first();

        if (!$unit)
            return ApiResponses::objectNotFound(__('Jednostka o podanym identyfikatorze nie istnieje'));

        $unit->delete();

        return ApiResponses::success();
    }
}
