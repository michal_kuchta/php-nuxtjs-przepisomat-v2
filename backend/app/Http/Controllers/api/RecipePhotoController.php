<?php

namespace App\Http\Controllers\api;

use App\Extensions\Responses\ApiResponses;
use App\Http\Controllers\Controller;
use App\Http\Requests\RecipePhotoStoreRequest;
use App\Http\Resources\RecipePhotoResource;
use App\Models\Recipe;
use App\Models\RecipePhoto;
use App\Models\RecipeStep;

class RecipePhotoController extends Controller
{
    /**
     * @OA\Get(
     *      path="/api/recipes/{recipeId}/photo",
     *      security={{"BearerAuth":{}}},
     *      tags={"Recipe Photos"},
     *      summary="Get list of recipe photos",
     *      @OA\Parameter(
     *          parameter="recipeId",
     *          name="recipeId",
     *          in="path",
     *          required=true,
     *          @OA\Schema(
     *              type="integer"
     *          )
     *      ),
     *      @OA\Response(
     *          response="200",
     *          description="Success",
     *          @OA\JsonContent(
     *              @OA\Property(
     *                  property="data",
     *                  type="array",
     *                  @OA\Items(
     *                      ref="#/components/schemas/RecipePhoto"
     *                  )
     *              )
     *          )
     *      ),
     *      @OA\Response(
     *          response="403",
     *          description="Forbidden",
     *          @OA\JsonContent(
     *              ref="#/components/schemas/ForbiddenError"
     *          )
     *      ),
     *      @OA\Response(
     *          response="404",
     *          description="Not found",
     *          @OA\JsonContent(
     *              ref="#/components/schemas/NotFoundError"
     *          )
     *      )
     * )
     * @param $recipeId
     * @return \Illuminate\Http\JsonResponse|\Illuminate\Http\Resources\Json\AnonymousResourceCollection
     */
    public function index($recipeId)
    {
        if (!Recipe::findQuery()->where('id', $recipeId)->exists())
            return ApiResponses::objectNotFound(__('Przepis o podanym identyfikatorze nie istnieje'));

        return RecipePhotoResource::collection(RecipePhoto::query()->where('recipe_id', $recipeId)->get());
    }

    /**
     * @OA\Get(
     *      path="/api/recipes/{recipeId}/photo/{id}",
     *      security={{"BearerAuth":{}}},
     *      tags={"Recipe Photos"},
     *      summary="Get recipe photo by Id",
     *      @OA\Parameter(
     *          parameter="recipeId",
     *          name="recipeId",
     *          in="path",
     *          required=true,
     *          @OA\Schema(
     *              type="integer"
     *          )
     *      ),
     *      @OA\Parameter(
     *          parameter="id",
     *          name="id",
     *          in="path",
     *          required=true,
     *          @OA\Schema(
     *              type="integer"
     *          )
     *      ),
     *      @OA\Response(
     *          response="200",
     *          description="Success",
     *          @OA\JsonContent(
     *              ref="#/components/schemas/RecipePhoto"
     *          )
     *      ),
     *      @OA\Response(
     *          response="403",
     *          description="Forbidden",
     *          @OA\JsonContent(
     *              ref="#/components/schemas/ForbiddenError"
     *          )
     *      ),
     *      @OA\Response(
     *          response="404",
     *          description="Not found",
     *          @OA\JsonContent(
     *              ref="#/components/schemas/NotFoundError"
     *          )
     *      )
     * )
     * @param $recipeId
     * @param $id
     * @return RecipePhotoResource|\Illuminate\Http\JsonResponse
     */
    public function show($recipeId, $id)
    {
        if (!Recipe::findQuery()->where('id', $recipeId)->exists())
            return ApiResponses::objectNotFound(__('Przepis o podanym identyfikatorze nie istnieje'));

        $photo = RecipePhoto::query()->where('id', $id)->where('recipe_id', $recipeId)->first();

        if (!$photo)
            return ApiResponses::objectNotFound(__('Zdjęcie o podanym identyfikatorze nie istnieje'));

        return new RecipePhotoResource($photo);
    }

    /**
     * @OA\Schema(
     *     schema="RecipePhotoRequest",
     *     required={"photo_id"},
     *     @OA\Property(
     *          property="photo_id",
     *          type="integer"
     *     )
     * )
     */

    /**
     * @OA\Post(
     *      path="/api/recipes/{recipeId}/photo",
     *      security={{"BearerAuth":{}}},
     *      tags={"Recipe Photos"},
     *      summary="Create new recipe photo",
     *      @OA\Parameter(
     *          parameter="recipeId",
     *          name="recipeId",
     *          in="path",
     *          required=true,
     *          @OA\Schema(
     *              type="integer"
     *          )
     *      ),
     *      @OA\RequestBody(
     *          required=true,
     *          @OA\JsonContent(
     *              ref="#/components/schemas/RecipePhotoRequest"
     *          )
     *      ),
     *      @OA\Response(
     *          response="201",
     *          description="Success",
     *          @OA\JsonContent(
     *              ref="#/components/schemas/RecipePhoto"
     *          )
     *      ),
     *      @OA\Response(
     *          response="403",
     *          description="Forbidden",
     *          @OA\JsonContent(
     *              ref="#/components/schemas/ForbiddenError"
     *          )
     *      ),
     *      @OA\Response(
     *          response="404",
     *          description="Not found",
     *          @OA\JsonContent(
     *              ref="#/components/schemas/NotFoundError"
     *          )
     *      ),
     *      @OA\Response(
     *          response="406",
     *          description="Validation errors",
     *          @OA\JsonContent(
     *              ref="#/components/schemas/ValidationErrors"
     *          )
     *      )
     * )
     * @param RecipePhotoStoreRequest $request
     * @param $recipeId
     * @return RecipePhotoResource|\Illuminate\Http\JsonResponse
     */
    public function store(RecipePhotoStoreRequest $request, $recipeId)
    {
        if (!Recipe::query()->where('id', $recipeId)->exists())
            return ApiResponses::objectNotFound(__('Przepis o podanym identyfikatorze nie istnieje'));

        if (!Recipe::query()->where('id', $recipeId)->where('user_id', auth()->id())->exists())
            return ApiResponses::forbidden(__('Nie jesteś autorem przepisu'));

        $request->validated();

        $photo = new RecipePhoto();
        $photo->fill([
            'recipe_id' => $recipeId,
            'photo_id' => $request->get('photo_id'),
        ]);
        $photo->save();

        return new RecipePhotoResource($photo);
    }

    /**
     * @OA\Put(
     *      path="/api/recipes/{recipeId}/photo/{id}",
     *      security={{"BearerAuth":{}}},
     *      tags={"Recipe Photos"},
     *      summary="Update recipe photo",
     *      @OA\Parameter(
     *          parameter="recipeId",
     *          name="recipeId",
     *          in="path",
     *          required=true,
     *          @OA\Schema(
     *              type="integer"
     *          )
     *      ),
     *      @OA\Parameter(
     *          parameter="id",
     *          name="id",
     *          in="path",
     *          required=true,
     *          @OA\Schema(
     *              type="integer"
     *          )
     *      ),
     *      @OA\RequestBody(
     *          required=true,
     *          @OA\JsonContent(
     *              ref="#/components/schemas/RecipePhotoRequest"
     *          )
     *      ),
     *      @OA\Response(
     *          response="201",
     *          description="Success",
     *          @OA\JsonContent(
     *              ref="#/components/schemas/RecipePhoto"
     *          )
     *      ),
     *      @OA\Response(
     *          response="403",
     *          description="Forbidden",
     *          @OA\JsonContent(
     *              ref="#/components/schemas/ForbiddenError"
     *          )
     *      ),
     *      @OA\Response(
     *          response="404",
     *          description="Not found",
     *          @OA\JsonContent(
     *              ref="#/components/schemas/NotFoundError"
     *          )
     *      ),
     *      @OA\Response(
     *          response="406",
     *          description="Validation errors",
     *          @OA\JsonContent(
     *              ref="#/components/schemas/ValidationErrors"
     *          )
     *      )
     * )
     * @param RecipePhotoStoreRequest $request
     * @param $recipeId
     * @param $id
     * @return RecipePhotoResource|\Illuminate\Http\JsonResponse
     */
    public function update(RecipePhotoStoreRequest $request, $recipeId, $id)
    {
        if (!Recipe::query()->where('id', $recipeId)->exists())
            return ApiResponses::objectNotFound(__('Przepis o podanym identyfikatorze nie istnieje'));

        if (!Recipe::query()->where('id', $recipeId)->where('user_id', auth()->id())->exists())
            return ApiResponses::forbidden(__('Nie jesteś autorem przepisu'));

        $request->validated();

        $photo = RecipePhoto::query()->where('id', $id)->where('recipe_id', $recipeId)->first();

        if (!$photo)
            return ApiResponses::objectNotFound(__('Zdjęcie o podanym identyfikatorze nie istnieje'));

        $photo->fill([
            'recipe_id' => $recipeId,
            'photo_id' => $request->get('photo_id'),
        ]);
        $photo->save();

        return new RecipePhotoResource($photo);
    }

    /**
     * @OA\Delete(
     *      path="/api/recipes/{recipeId}/photo/{id}",
     *      security={{"BearerAuth":{}}},
     *      tags={"Recipe Photos"},
     *      summary="Delete recipe photo",
     *      @OA\Parameter(
     *          parameter="recipeId",
     *          name="recipeId",
     *          in="path",
     *          required=true,
     *          @OA\Schema(
     *              type="integer"
     *          )
     *      ),
     *      @OA\Parameter(
     *          parameter="id",
     *          name="id",
     *          in="path",
     *          required=true,
     *          @OA\Schema(
     *              type="integer"
     *          )
     *      ),
     *      @OA\Response(
     *          response="200",
     *          description="Success"
     *      ),
     *      @OA\Response(
     *          response="403",
     *          description="Forbidden",
     *          @OA\JsonContent(
     *              ref="#/components/schemas/ForbiddenError"
     *          )
     *      ),
     *      @OA\Response(
     *          response="404",
     *          description="Not found",
     *          @OA\JsonContent(
     *              ref="#/components/schemas/NotFoundError"
     *          )
     *      )
     * )
     * @param $recipeId
     * @param $id
     * @return \Illuminate\Http\JsonResponse
     * @throws \Exception
     */
    public function delete($recipeId, $id)
    {
        if (!Recipe::query()->where('id', $recipeId)->exists())
            return ApiResponses::objectNotFound(__('Przepis o podanym identyfikatorze nie istnieje'));

        if (!Recipe::query()->where('id', $recipeId)->where('user_id', auth()->id())->exists())
            return ApiResponses::forbidden(__('Nie jesteś autorem przepisu'));

        $photo = RecipeStep::query()->where('id', $id)->where('recipe_id', $recipeId)->first();

        if (!$photo)
            return ApiResponses::objectNotFound(__('Zdjęcie o podanym identyfikatorze nie istnieje'));

        $photo->delete();

        return ApiResponses::success();
    }
}
