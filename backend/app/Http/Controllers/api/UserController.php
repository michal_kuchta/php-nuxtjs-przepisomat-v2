<?php

namespace App\Http\Controllers\api;

use App\Extensions\Responses\ApiResponses;
use App\Http\Controllers\Controller;
use App\Http\Requests\UserPasswordUpdateRequest;
use App\Http\Requests\UserSettingsUpdateRequest;
use App\Http\Requests\UserUpdateRequest;
use App\Http\Resources\UserResource;
use App\Models\Recipe;
use App\Models\RecipeShare;
use App\Models\UserShare;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;

class UserController extends Controller
{
    /**
     * @OA\Get(
     *      path="/api/me",
     *      summary="Get information about logged user",
     *      security={{"BearerAuth":{}}},
     *      tags={"Users"},
     *      @OA\Response(
     *          response="200",
     *          description="",
     *          @OA\JsonContent(
     *              ref="#/components/schemas/User"
     *          )
     *      )
     * )
     * @return UserResource
     */
    public function getMe()
    {
        return new UserResource(auth()->user());
    }

    /**
     * @OA\Put(
     *      path="/api/users",
     *      security={{"BearerAuth":{}}},
     *      tags={"Users"},
     *      summary="Edit user data",
     *      @OA\RequestBody(
     *          required=true,
     *          @OA\JsonContent(
     *              ref="#/components/schemas/UserUpdateRequest"
     *          )
     *      ),
     *      @OA\Response(
     *          response="200",
     *          description="Success - user anonymized"
     *      ),
     *      @OA\Response(
     *          response="201",
     *          description="Success",
     *          @OA\JsonContent(
     *              ref="#/components/schemas/User"
     *          )
     *      ),
     *      @OA\Response(
     *          response="406",
     *          description="Validation errors",
     *          @OA\JsonContent(
     *              ref="#/components/schemas/ValidationErrors"
     *          )
     *      )
     * )
     * @param UserUpdateRequest $request
     * @return RecipeResource|UserResource|\Illuminate\Http\JsonResponse
     */
    public function update(UserUpdateRequest $request)
    {
        $credentials = ['email' => auth()->user()->email, 'password' => request()->get('password')];

        if (!auth()->validate($credentials))
        {
            return ApiResponses::validationErrors('', ['password' => "Błędne hasło"]);
        }

        $request->validated();

        $user = auth()->user();
        $user->fill(request()->only(['email', 'name']));
        $user->is_rodo_checked = $request->get('is_rodo_checked', true);

        if ($user->isDirty('is_rodo_checked') && !$user->is_rodo_checked)
        {
            $user->save();
            $user->anonymize();

            return ApiResponses::success(201);
        }

        $user->save();

        return new UserResource($user);
    }

    /**
     * @OA\Put(
     *      path="/api/users/password",
     *      security={{"BearerAuth":{}}},
     *      tags={"Users"},
     *      summary="Edit user password",
     *      @OA\RequestBody(
     *          required=true,
     *          @OA\JsonContent(
     *              ref="#/components/schemas/UserPasswordUpdateRequest"
     *          )
     *      ),
     *      @OA\Response(
     *          response="200",
     *          description="Success",
     *          @OA\JsonContent(
     *              ref="#/components/schemas/User"
     *          )
     *      ),
     *      @OA\Response(
     *          response="406",
     *          description="Validation errors",
     *          @OA\JsonContent(
     *              ref="#/components/schemas/ValidationErrors"
     *          )
     *      )
     * )
     * @param UserPasswordUpdateRequest $request
     * @return UserResource|\Illuminate\Http\JsonResponse
     */
    public function updatePassword(UserPasswordUpdateRequest $request)
    {
        $credentials = ['email' => auth()->user()->email, 'password' => request()->get('password')];

        if (!auth()->validate($credentials))
        {
            return ApiResponses::validationErrors('', ['password' => "Błędne hasło"]);
        }

        $request->validated();

        $user = auth()->user();
        $user->password = Hash::make(request()->get('new_password'));
        $user->save();

        return new UserResource($user);
    }

    /**
     * @OA\Put(
     *      path="/api/users/settings",
     *      security={{"BearerAuth":{}}},
     *      tags={"Users"},
     *      summary="Edit user settings",
     *      @OA\RequestBody(
     *          required=true,
     *          @OA\JsonContent(
     *              ref="#/components/schemas/UserSettingsUpdateRequest"
     *          )
     *      ),
     *      @OA\Response(
     *          response="200",
     *          description="Success",
     *          @OA\JsonContent(
     *              ref="#/components/schemas/User"
     *          )
     *      ),
     *      @OA\Response(
     *          response="406",
     *          description="Validation errors",
     *          @OA\JsonContent(
     *              ref="#/components/schemas/ValidationErrors"
     *          )
     *      )
     * )
     * @param UserSettingsUpdateRequest $request
     * @return UserResource|\Illuminate\Http\JsonResponse
     */
    public function updateSettings(UserSettingsUpdateRequest $request)
    {
        UserShare::query()->where('object_id', auth()->id())->delete();

        $user = auth()->user();
        $user->share_type = $request->get('share_type', 0);
        $user->save();

        if ($request->get('share_type', 0) == Recipe::SHARE_TYPE_MAILS)
        {
            foreach ($request->get('shared_emails', []) as $sharedEmailRequest)
            {
                $sharedEmail = new UserShare();
                $sharedEmail->fill([
                    'object_id' => auth()->id(),
                    'email' => $sharedEmailRequest['value'],
                    'type' => Recipe::SHARE_TYPE_MAILS
                ]);
                $sharedEmail->save();
            }
        }
        elseif ($request->get('share_type', 0) == Recipe::SHARE_TYPE_ALL)
        {
            $sharedEmail = new UserShare();
            $sharedEmail->fill([
                'object_id' => auth()->id(),
                'email' => '',
                'type' => Recipe::SHARE_TYPE_ALL
            ]);
            $sharedEmail->save();
        }

        return new UserResource($user);
    }

    /**
     * @OA\Delete(
     *      path="/api/users",
     *      security={{"BearerAuth":{}}},
     *      tags={"Users"},
     *      summary="Delete user",
     *      @OA\Response(
     *          response="200",
     *          description="Success"
     *      ),
     *      @OA\Response(
     *          response="406",
     *          description="Validation errors",
     *          @OA\JsonContent(
     *              ref="#/components/schemas/ValidationErrors"
     *          )
     *      )
     * )
     * @param $id
     * @return \Illuminate\Http\JsonResponse
     * @throws \Exception
     */
    public function delete()
    {
        $credentials = ['email' => auth()->user()->email, 'password' => request()->get('password')];

        if (!auth()->validate($credentials))
        {
            return ApiResponses::validationErrors('', ['password' => "Błędne hasło"]);
        }

        auth()->user()->anonymize();

        return ApiResponses::success();
    }
}
