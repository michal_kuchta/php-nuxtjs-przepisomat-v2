<?php

namespace App\Http\Controllers\api;

use App\Extensions\Responses\ApiResponses;
use App\Http\Controllers\Controller;
use App\Http\Resources\PhotoResource;
use App\Models\Photo;
use Faker\Provider\File;
use Illuminate\Support\Arr;
use Intervention\Image\Facades\Image;

class PhotosController extends Controller
{
    /**
     * @OA\Get(
     *      path="/api/photos",
     *      security={{"BearerAuth":{}}},
     *      tags={"Photos"},
     *      summary="Get photos list",
     *      @OA\Response(
     *          response="200",
     *          description="Success",
     *          @OA\JsonContent(
     *              @OA\Property(
     *                  property="data",
     *                      type="array",
     *                  @OA\Items(
     *                      ref="#/components/schemas/Photo"
     *                  )
     *              )
     *          )
     *      )
     * )
     */
    public function index()
    {
        return PhotoResource::collection(Photo::query()->where('user_id', auth()->id())->get());
    }

    /**
     * @OA\Get(
     *      path="/api/photos/{id}",
     *      security={{"BearerAuth":{}}},
     *      tags={"Photos"},
     *      summary="Get photo by id",
     *      @OA\Parameter(
     *          parameter="id",
     *          name="id",
     *          in="path",
     *          required=true,
     *          @OA\Schema(
     *              type="integer"
     *          )
     *      ),
     *      @OA\Response(
     *          response="200",
     *          description="Success",
     *          @OA\JsonContent(
     *              ref="#/components/schemas/Photo"
     *          )
     *      ),
     *      @OA\Response(
     *          response="403",
     *          description="Forbidden",
     *          @OA\JsonContent(
     *              ref="#/components/schemas/ForbiddenError"
     *          )
     *      ),
     *      @OA\Response(
     *          response="404",
     *          description="Photo not found",
     *          @OA\JsonContent(
     *              ref="#/components/schemas/NotFoundError"
     *          )
     *      )
     * )
     *
     * @param $id
     * @return PhotoResource|\Illuminate\Http\JsonResponse
     */
    public function show($id)
    {
        if (!Photo::query()->where('user_id', $id)->exists())
            return ApiResponses::forbidden(__('Nie jesteś autorem tego zdjęcia'));

        $photo = Photo::find($id);

        if (!$photo)
            return ApiResponses::objectNotFound(__('Nie znaleziono zdjęcia o podanym identyfikatorze'));

        return new PhotoResource($photo);
    }

    /**
     * @OA\Post(
     *      path="/api/photos",
     *      security={{"BearerAuth":{}}},
     *      tags={"Photos"},
     *      summary="Create new photo",
     *      @OA\RequestBody(
     *          required=true,
     *          @OA\MediaType(
     *              mediaType="multipart/form-data",
     *              @OA\Schema(
     *                  @OA\Property(
     *                      property="photo",
     *                      type="string",
     *                      format="base64"
     *                  ),
     *              ),
     *          )
     *      ),
     *      @OA\Response(
     *          response="201",
     *          description="Success",
     *          @OA\JsonContent(
     *              ref="#/components/schemas/Photo"
     *          )
     *      ),
     *      @OA\Response(
     *          response="400",
     *          description="Error",
     *          @OA\JsonContent(
     *              @OA\Property(
     *                  property="error",
     *                  type="string",
     *                  example="File is missing"
     *              )
     *          )
     *      )
     * )
     */
    public function store()
    {
        if(request()->hasFile('photo'))
        {
            $dir = storage_path('/app/public/recipes');
            $tempDir = storage_path('/app/public/temp');
            if (!file_exists($dir))
            {
                mkdir($dir,0777,true);
            }

            if (!file_exists($tempDir))
            {
                mkdir($tempDir,0777,true);
            }

            $image = request()->file('photo');
            $imageName = $image->hashName();
            $image->move($tempDir, $imageName);

            $img = Image::make($tempDir.'/'.$imageName);
            $img->resize(500, null, function($constraint){
                $constraint->aspectRatio();
            })->save($dir . '/' . $imageName);

            $path = '/storage/recipes/' . $imageName;

            $photo = new Photo();
            $photo->url = $path;
            $photo->user_id = auth()->id();
            $photo->save();

            return new PhotoResource($photo);
        }

        return response()->json([
            "error" => __('Brak pliku')
        ], 406);
    }

    /**
     * @OA\Put(
     *      path="/api/photos/{id}",
     *      security={{"BearerAuth":{}}},
     *      tags={"Photos"},
     *      summary="Update photo",
     *      @OA\Parameter(
     *          parameter="id",
     *          name="id",
     *          in="path",
     *          required=true,
     *          @OA\Schema(
     *              type="integer"
     *          )
     *      ),
     *      @OA\RequestBody(
     *          required=true,
     *          @OA\MediaType(
     *              mediaType="multipart/form-data",
     *              @OA\Schema(
     *                  @OA\Property(
     *                      property="photo",
     *                      type="string",
     *                      format="base64"
     *                  ),
     *              ),
     *          )
     *      ),
     *      @OA\Response(
     *          response="201",
     *          description="Success",
     *          @OA\JsonContent(
     *              ref="#/components/schemas/Photo"
     *          )
     *      ),
     *      @OA\Response(
     *          response="400",
     *          description="Error",
     *          @OA\JsonContent(
     *              @OA\Property(
     *                  property="error",
     *                  type="string",
     *                  example="File is missing"
     *              )
     *          )
     *      )
     * )
     * @param $id
     * @return PhotoResource|\Illuminate\Http\JsonResponse
     */
    public function update($id)
    {
        $photo = Photo::query()->where('user_id', auth()->id())->find($id);

        if (!$photo)
            return ApiResponses::objectNotFound(__('Zdjęcie o podanym identyfikatorze nie isteniej'));

        if(request()->hasFile('photo'))
        {
            $dir = storage_path('/app/public/recipes');
            $tempDir = storage_path('/app/public/temp');
            if (!file_exists($dir))
            {
                mkdir($dir,0777,true);
            }

            if (!file_exists($tempDir))
            {
                mkdir($tempDir,0777,true);
            }

            $image = request()->file('photo');
            $imageName = $image->hashName();
            $image->move($tempDir, $imageName);

            $oldPhotoName = Arr::last(explode('/', $photo->url));

            $img = Image::make($tempDir.'/'.$imageName);
            $img->resize(500, null, function($constraint){
                $constraint->aspectRatio();
            })->save($dir . '/' . $oldPhotoName);

            \Illuminate\Support\Facades\File::delete($tempDir.'/'.$imageName);

            return new PhotoResource($photo);
        }

        return response()->json([
            "error" => __('Brak pliku')
        ], 406);
    }

    /**
     * @OA\Delete(
     *      path="/api/photos/{id}",
     *      security={{"BearerAuth":{}}},
     *      tags={"Photos"},
     *      summary="Delete photo",
     *      @OA\Parameter(
     *          parameter="id",
     *          name="id",
     *          in="path",
     *          required=true,
     *          @OA\Schema(
     *              type="integer"
     *          )
     *      ),
     *      @OA\Response(
     *          response="200",
     *          description="Success"
     *      ),
     *      @OA\Response(
     *          response="404",
     *          description="Not found",
     *          @OA\JsonContent(
     *              ref="#/components/schemas/NotFoundError"
     *          )
     *      )
     * )
     * @param $id
     * @return \Illuminate\Http\JsonResponse
     * @throws \Exception
     */
    public function delete($id)
    {
        $photo = Photo::query()->where('user_id', auth()->id())->find($id);

        if (!$photo)
            return ApiResponses::objectNotFound(__('Zdjęcie o podanym identyfikatorze nie isteniej'));

        $dir = storage_path('/app/public/recipes');
        $imageName = Arr::last(explode('/', $photo->url));

        \Illuminate\Support\Facades\File::delete($dir.'/'.$imageName);
        $photo->delete();

        return ApiResponses::success();
    }
}
