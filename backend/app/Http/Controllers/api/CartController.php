<?php

namespace App\Http\Controllers\api;

use App\Extensions\Responses\ApiResponses;
use App\Http\Controllers\Controller;
use App\Http\Requests\CartStoreRequest;
use App\Http\Resources\CartCollection;
use App\Http\Resources\CartResource;
use App\Models\CartItem;
use App\Models\Recipe;
use App\Models\RecipeIngredient;

class CartController extends Controller
{
    /**
     * @OA\Get(
     *      path="/api/cart",
     *      security={{"BearerAuth":{}}},
     *      tags={"Cart"},
     *      summary="Get list of cart ingredients",
     *      @OA\Response(
     *          response="200",
     *          description="Success",
     *          @OA\JsonContent(
     *              @OA\Property(
     *                  property="data",
     *                  type="array",
     *                  @OA\Items(
     *                      ref="#/components/schemas/Cart"
     *                  )
     *              )
     *          )
     *      )
     * )
     * @return CartCollection
     */
    public function index()
    {
        return new CartCollection(CartItem::query()->where('user_id', auth()->id())->get());
    }

    /**
     * @OA\Post(
     *      path="/api/cart",
     *      security={{"BearerAuth":{}}},
     *      tags={"Cart"},
     *      summary="Add ingredient into cart",
     *      @OA\RequestBody(
     *          required=true,
     *          @OA\JsonContent(
     *              ref="#/components/schemas/CartStoreRequest"
     *          )
     *      ),
     *      @OA\Response(
     *          response="201",
     *          description="Success",
     *          @OA\JsonContent(
     *              ref="#/components/schemas/CartItem"
     *          )
     *      ),
     *      @OA\Response(
     *          response="403",
     *          description="Forbidden",
     *          @OA\JsonContent(
     *              ref="#/components/schemas/ForbiddenError"
     *          )
     *      ),
     *      @OA\Response(
     *          response="404",
     *          description="Not found",
     *          @OA\JsonContent(
     *              ref="#/components/schemas/NotFoundError"
     *          )
     *      ),
     *      @OA\Response(
     *          response="406",
     *          description="Validation errors",
     *          @OA\JsonContent(
     *              ref="#/components/schemas/ValidationErrors"
     *          )
     *      )
     * )
     * @param CartStoreRequest $request
     * @return CartResource|\Illuminate\Http\JsonResponse
     */
    public function store(CartStoreRequest $request)
    {
        if (!Recipe::query()->where('id', request()->input('recipe_id'))->where('user_id', auth()->id())->exists())
            return ApiResponses::forbidden(__('Przepis o podanym identyfikatorze nie istnieje lub nie jesteś autorem przepisu'));

        if (!RecipeIngredient::query()->where('id', request()->input('ingredient_id'))->where('recipe_id', request()->input('recipe_id'))->exists())
            return ApiResponses::forbidden(__('Składnik o podanym identyfikatorze nie znajduje się w przepisie'));

        $request->validated();

        $entity = new CartItem();
        $entity->fill([
            'recipe_id' => $request->get('recipe_id'),
            'ingredient_id' => $request->get('ingredient_id'),
            'user_id' => auth()->id()
        ]);
        $entity->save();

        return new CartResource($entity);
    }

    /**
     * @OA\Delete(
     *      path="/api/cart/{id}",
     *      security={{"BearerAuth":{}}},
     *      tags={"Cart"},
     *      summary="Remove item from cart",
     *      @OA\Parameter(
     *          parameter="id",
     *          name="id",
     *          in="path",
     *          required=true,
     *          @OA\Schema(
     *              type="integer"
     *          )
     *      ),
     *      @OA\Response(
     *          response="200",
     *          description="Success"
     *      ),
     *      @OA\Response(
     *          response="403",
     *          description="Forbidden",
     *          @OA\JsonContent(
     *              ref="#/components/schemas/ForbiddenError"
     *          )
     *      ),
     *      @OA\Response(
     *          response="404",
     *          description="Not found",
     *          @OA\JsonContent(
     *              ref="#/components/schemas/NotFoundError"
     *          )
     *      )
     * )
     * @param $id
     * @return \Illuminate\Http\JsonResponse
     * @throws \Exception
     */
    public function delete($id)
    {
        if (!CartItem::query()->where('id', $id)->where('user_id', auth()->id())->exists())
            return ApiResponses::objectNotFound(__('Pozycja koszyka o podanym identyfikatorze nie istnieje'));

        CartItem::query()->where('id', $id)->where('user_id', auth()->id())->delete();

        return ApiResponses::success();
    }

    /**
     * @OA\Delete(
     *      path="/api/cart/{recipe_id}/{ingredient_id}",
     *      security={{"BearerAuth":{}}},
     *      tags={"Cart"},
     *      summary="Remove first item from cart by recipe and ingredient ids",
     *      @OA\Parameter(
     *          parameter="recipe_id",
     *          name="recipe_id",
     *          in="path",
     *          required=true,
     *          @OA\Schema(
     *              type="integer"
     *          )
     *      ),
     *      @OA\Parameter(
     *          parameter="ingredient_id",
     *          name="ingredient_id",
     *          in="path",
     *          required=true,
     *          @OA\Schema(
     *              type="integer"
     *          )
     *      ),
     *      @OA\Response(
     *          response="200",
     *          description="Success"
     *      ),
     *      @OA\Response(
     *          response="403",
     *          description="Forbidden",
     *          @OA\JsonContent(
     *              ref="#/components/schemas/ForbiddenError"
     *          )
     *      ),
     *      @OA\Response(
     *          response="404",
     *          description="Not found",
     *          @OA\JsonContent(
     *              ref="#/components/schemas/NotFoundError"
     *          )
     *      )
     * )
     * @param $recipe_id
     * @param $ingredient_id
     * @return \Illuminate\Http\JsonResponse
     * @throws \Exception
     */
    public function deleteOne($recipe_id, $ingredient_id)
    {
        if (!CartItem::query()->where('recipe_id', $recipe_id)->where('ingredient_id', $ingredient_id)->where('user_id', auth()->id())->exists())
            return ApiResponses::objectNotFound(__('Pozycja koszyka o podanym identyfikatorze nie istnieje'));

        CartItem::query()
            ->where('recipe_id', $recipe_id)
            ->where('ingredient_id', $ingredient_id)
            ->where('user_id', auth()->id())
            ->first()
            ->delete();

        return ApiResponses::success();
    }
}
