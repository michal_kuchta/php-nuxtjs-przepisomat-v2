<?php

namespace App\Http\Controllers\api\Auth;

use App\Http\Controllers\Controller;
use App\Providers\RouteServiceProvider;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Http\Request;
use Tymon\JWTAuth\Exceptions\JWTException;
use Tymon\JWTAuth\JWTAuth;

class LoginController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */

    use AuthenticatesUsers;

    /**
     * Where to redirect users after login.
     *
     * @var string
     */
    protected $redirectTo = '';

    /**
     * @OA\Post(
     *      path="/api/auth/login",
     *      tags={"Auth"},
     *      operationId="login",
     *      summary="Login",
     *      @OA\RequestBody(
     *          required=true,
     *          @OA\JsonContent(
     *              required={"email","password"},
     *              @OA\Property(
     *                  property="email",
     *                  type="string",
     *                  format="email",
     *                  example="email@example.com"
     *              ),
     *              @OA\Property(
     *                  property="password",
     *                  type="string",
     *                  format="password",
     *                  example="123456789"
     *              )
     *          )
     *      ),
     *      @OA\Response(
     *          response="200",
     *          description="Logged in",
     *          @OA\JsonContent(
     *              @OA\Property(
     *                  property="succes",
     *                  type="boolean",
     *                  example="true"
     *              ),
     *              @OA\Property(
     *                  property="data",
     *                  type="object",
     *                  ref="#/components/schemas/User"
     *              ),
     *              @OA\Property(
     *                  property="token",
     *                  type="string"
     *              )
     *          )
     *      ),
     *      @OA\Response(
     *          response="422",
     *          description="Wrong email or password",
     *          @OA\JsonContent(
     *              @OA\Property(
     *                  property="succes",
     *                  type="boolean",
     *                  example="false"
     *              ),
     *              @OA\Property(
     *                  property="errors",
     *                  type="array",
     *                  @OA\Items(
     *                      type="string",
     *                      example="Wrong email or password"
     *                  )
     *              )
     *          )
     *      ),
     *      @OA\Response(
     *          response="429",
     *          description="You account has been blocked",
     *          @OA\JsonContent(
     *              @OA\Property(
     *                  property="succes",
     *                  type="boolean",
     *                  example="false"
     *              ),
     *              @OA\Property(
     *                  property="errors",
     *                  type="array",
     *                  example="You account has been blocked",
     *                  @OA\Items(
     *                      type="string",
     *                      example="Wrong email or password"
     *                  )
     *              )
     *          )
     *      )
     * )
     */
    public function postLogin(Request $request)
    {
        // If the class is using the ThrottlesLogins trait, we can automatically throttle
        // the login attempts for this application. We'll key this by the username and
        // the IP address of the client making these requests into this application.
        if (method_exists($this, 'hasTooManyLoginAttempts') &&
            $this->hasTooManyLoginAttempts($request)) {
            $this->fireLockoutEvent($request);

            return response()->json([
                'success' => false,
                'errors' => [
                    __("Twoje konto zostało zablokowane")
                ]
            ], 429);
        }

        // If the login attempt was unsuccessful we will increment the number of attempts
        // to login and redirect the user back to the login form. Of course, when this
        // user surpasses their maximum number of attempts they will get locked out.
        $this->incrementLoginAttempts($request);

        try
        {
            if (!$token = $this->auth->attempt($request->only('email', 'password')))
            {
                return response()->json([
                    'succes' => false,
                    'errors' => [
                        'email' => [
                            __('Niepoprawny adres email lub hasło')
                        ],
                        'password' => [
                            __('Niepoprawny adres email lub hasło')
                        ]
                    ]
                ], 422);
            }

            if (!$request->user()->is_active)
            {
                return response()->json([
                    'succes' => false,
                    'error' => __('Twoje konto nie zostało jeszcze aktywowane przez administratora systemu.')
                ], 403);
            }
        }
        catch (JWTException $e)
        {
            return response()->json([
                'succes' => false,
                'errors' => [
                    'email' => [
                        __('Niepoprawny adres email lub hasło')
                    ],
                    'password' => [
                        __('Niepoprawny adres email lub hasło')
                    ]
                ]
            ], 422);
        }

        return $this->respondWithToken($request, $token);
    }

    private function respondWithToken(Request $request, $token)
    {
        return response()->json([
            'success' => true,
            'data' => $request->user(),
            'token' => $token
        ], 200);
    }
}
