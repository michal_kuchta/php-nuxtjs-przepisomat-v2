<?php

namespace App\Http\Controllers\api\Auth;

use App\Http\Controllers\Controller;
use Tymon\JWTAuth\JWTAuth;

class LogoutController extends Controller
{
    /**
     * @OA\Post(
     *      path="/api/auth/logout",
     *      tags={"Auth"},
     *      operationId="logout",
     *      summary="Logout",
     *      @OA\Response(
     *          response="200",
     *          description="Logged out",
     *          @OA\JsonContent(
     *              @OA\Property(
     *                  property="succes",
     *                  type="boolean",
     *                  example="true"
     *              )
     *          )
     *      )
     * )
     */
    public function getLogout()
    {
        $this->auth->invalidate();

        return response()->json([
            'success' => true
        ]);
    }
}
