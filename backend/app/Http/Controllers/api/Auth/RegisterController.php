<?php

namespace App\Http\Controllers\api\Auth;

use App\Http\Controllers\Controller;
use App\Providers\RouteServiceProvider;
use App\Models\User;
use Illuminate\Auth\Events\Registered;
use Illuminate\Foundation\Auth\RegistersUsers;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use Tymon\JWTAuth\JWTAuth;

class RegisterController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Register Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles the registration of new users as well as their
    | validation and creation. By default this controller uses a trait to
    | provide this functionality without requiring any additional code.
    |
    */

    use RegistersUsers;

    /**
     * Where to redirect users after registration.
     *
     * @var string
     */
    protected $redirectTo = '';

    /**
     * Get a validator for an incoming registration request.
     *
     * @param  array  $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function validator(array $data)
    {
        return Validator::make($data, [
            'name' => ['required', 'string', 'max:255'],
            'email' => ['required', 'string', 'email', 'max:255', 'unique:users'],
            'password' => ['required', 'string', 'min:5', 'confirmed'],
            'is_rodo_checked' => ['required']
        ],
        [
            'name.required' => __('Imię i Nazwisko jest wymagane'),
            'name.string' => __('Niepoprawna wartość pola Imię i Nazwisko'),
            'name.max' => __('Wartość pola Imię i nazwisko jest zbyt długa'),
            'email.required' => __('Adres e-mail jest wymagany'),
            'email.string' => __('Adres e-mail jest nieprawidłowy lub juz istnieje konto o takim adresie'),
            'email.email' => __('Adres e-mail jest nieprawidłowy lub juz istnieje konto o takim adresie'),
            'email.max' => __('Zbyt długi adres e-mail'),
            'email.unique:users' => __('Adres e-mail jest nieprawidłowy lub juz istnieje konto o takim adresie'),
            'password.required' => __('Hasło jest wymagane'),
            'password.string' => __('Niepoprawne hasło'),
            'password.min' => __('Hasło powinno składać się z minimum :min znaków'),
            'password.confirmed' => __('Podane hasła różnią się od siebie'),
            'is_rodo_checked.required' => __('Zaznaczenie zgody jest wymagane'),
        ]);
    }

    /**
     * Create a new user instance after a valid registration.
     *
     * @param  array  $data
     * @return \App\Models\User
     */
    protected function create(array $data)
    {
        return User::create([
            'name' => $data['name'],
            'email' => $data['email'],
            'password' => Hash::make($data['password']),
            'is_rodo_checked' => $data['is_rodo_checked'],
            'is_active' => false,
            'is_admin' => false
        ]);
    }


    /**
     * @OA\Post(
     *      path="/api/auth/register",
     *      tags={"Auth"},
     *      operationId="register",
     *      summary="Register",
     *      @OA\RequestBody(
     *          required=true,
     *          @OA\JsonContent(
     *              required={"email","password"},
     *              @OA\Property(
     *                  property="email",
     *                  type="string",
     *                  format="email",
     *                  example="email@example.com"
     *              ),
     *              @OA\Property(
     *                  property="password",
     *                  type="string",
     *                  format="password",
     *                  example="123456789"
     *              )
     *          )
     *      ),
     *      @OA\Response(
     *          response="201",
     *          description="Logged in",
     *          @OA\JsonContent(
     *              @OA\Property(
     *                  property="succes",
     *                  type="boolean",
     *                  example="true"
     *              ),
     *              @OA\Property(
     *                  property="data",
     *                  type="object",
     *                  ref="#/components/schemas/User"
     *              ),
     *              @OA\Property(
     *                  property="token",
     *                  type="string"
     *              )
     *          )
     *      ),
     *      @OA\Response(
     *          response="422",
     *          description="Wrong email or password",
     *          @OA\JsonContent(
     *              @OA\Property(
     *                  property="succes",
     *                  type="boolean",
     *                  example="false"
     *              ),
     *              @OA\Property(
     *                  property="errors",
     *                  type="array",
     *                  @OA\Items(
     *                      type="string",
     *                      example="Wrong email or password"
     *                  )
     *              )
     *          )
     *      ),
     *      @OA\Response(
     *          response="429",
     *          description="You account has been blocked",
     *          @OA\JsonContent(
     *              @OA\Property(
     *                  property="succes",
     *                  type="boolean",
     *                  example="false"
     *              ),
     *              @OA\Property(
     *                  property="errors",
     *                  type="array",
     *                  example="You account has been blocked",
     *                  @OA\Items(
     *                      type="string",
     *                      example="Wrong email or password"
     *                  )
     *              )
     *          )
     *      )
     * )
     */
    public function postRegister(Request $request)
    {
        $input = $request->all();
        if (!$input['is_rodo_checked'])
            unset($input['is_rodo_checked']);

        $validator = $this->validator($input);

        if (!$validator->fails())
        {
            $user = $this->create($request->all());
            $token = $this->auth->attempt($request->only('email', 'password', 'is_rodo_checked'));

            return response()->json([
                'success' => true,
                'data' => $user,
                'token' => $token
            ], 200);
        }

        return response()->json([
            'success' => false,
            'errors' => $validator->errors()
        ], 422);
    }
}
