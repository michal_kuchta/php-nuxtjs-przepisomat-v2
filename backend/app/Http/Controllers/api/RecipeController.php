<?php

namespace App\Http\Controllers\api;

use App\Extensions\Responses\ApiResponses;
use App\Http\Controllers\Controller;
use App\Http\Requests\RecipeStoreRequest;
use App\Http\Resources\RecipeRateResource;
use App\Http\Resources\RecipeResource;
use App\Models\Recipe;
use App\Models\RecipeIngredient;
use App\Models\RecipePhoto;
use App\Models\RecipeRate;
use App\Models\RecipeShare;
use App\Models\RecipeStep;
use App\Models\UserFavoriteRecipe;

class RecipeController extends Controller
{
    /**
     * @OA\Get(
     *      path="/api/recipes",
     *      security={{"BearerAuth":{}}},
     *      tags={"Recipes"},
     *      summary="Get recipes list",
     *      @OA\Parameter(
     *          parameter="page",
     *          name="page",
     *          in="query",
     *          @OA\Schema(
     *              type="integer",
     *              default=1
     *          )
     *      ),
     *      @OA\Parameter(
     *          parameter="perPage",
     *          name="perPage",
     *          in="query",
     *          @OA\Schema(
     *              type="integer",
     *              default=10
     *          )
     *      ),
     *      @OA\Parameter(
     *          parameter="orderBy",
     *          name="orderBy",
     *          in="query",
     *          @OA\Schema(
     *              type="string",
     *              default="date"
     *          )
     *      ),
     *      @OA\Parameter(
     *          parameter="orderDirection",
     *          name="orderDirection",
     *          in="query",
     *          @OA\Schema(
     *              type="string",
     *              default="desc"
     *          )
     *      ),
     *      @OA\Parameter(
     *          parameter="query",
     *          name="query",
     *          in="query",
     *          @OA\Schema(
     *              type="string",
     *              default=""
     *          )
     *      ),
     *      @OA\Parameter(
     *          parameter="categoryId",
     *          name="categoryId",
     *          in="query",
     *          @OA\Schema(
     *              type="integer",
     *              default=""
     *          )
     *      ),
     *      @OA\Response(
     *          response="200",
     *          description="Success",
     *          @OA\JsonContent(
     *              @OA\Property(
     *                  property="data",
     *                  type="array",
     *                  @OA\Items(
     *                      ref="#/components/schemas/Recipe"
     *                  )
     *              ),
     *              @OA\Property(
     *                  property="links",
     *                  type="object",
     *                      ref="#/components/schemas/PagerLinks"
     *              ),
     *              @OA\Property(
     *                  property="meta",
     *                  type="object",
     *                      ref="#/components/schemas/PagerMeta"
     *              )
     *          )
     *      )
     * )
     */
    public function index()
    {
        $perPage = request()->get('perPage', 10);
        $page = request()->get('page', 1);
        $query = trim(request()->get('query', ''));
        $categoryId = intval(request()->get('categoryId', 0));

        $orderBy = request()->get('orderBy', 'date');
        if (!in_array($orderBy, ['date', 'name']))
            $orderBy = 'date';

        switch ($orderBy)
        {
            case "date":
                $orderBy = 'created_at';
        }

        $orderDirection = request()->get('orderDirection', 'desc');
        if (!in_array($orderDirection, ['asc', 'desc']))
            $orderDirection = 'desc';

        return RecipeResource::collection(Recipe::findQuery($query, $categoryId)
            ->orderBy($orderBy, $orderDirection)
            ->paginate($perPage, ['*'], 'page', $page));
    }

    /**
     * @OA\Get(
     *      path="/api/recipes/{id}",
     *      security={{"BearerAuth":{}}},
     *      tags={"Recipes"},
     *      summary="Get recipe by id",
     *      @OA\Parameter(
     *          parameter="id",
     *          name="id",
     *          in="path",
     *          required=true,
     *          @OA\Schema(
     *              type="integer"
     *          )
     *      ),
     *      @OA\Response(
     *          response="200",
     *          description="Success",
     *          @OA\JsonContent(
     *              ref="#/components/schemas/Recipe"
     *          )
     *      ),
     *      @OA\Response(
     *          response="404",
     *          description="Ingredient not found",
     *          @OA\JsonContent(
     *              ref="#/components/schemas/NotFoundError"
     *          )
     *      )
     * )
     *
     * @param $id
     * @return RecipeResource|\Illuminate\Http\JsonResponse
     */
    public function show($id)
    {
        $recipe = Recipe::findQuery()->find($id);

        if (!$recipe)
            return ApiResponses::objectNotFound(__('Przepis o podanym identyfikatorze nie istnieje'));

        return new RecipeResource($recipe);
    }

    /**
     * @OA\Post(
     *      path="/api/recipes",
     *      security={{"BearerAuth":{}}},
     *      tags={"Recipes"},
     *      summary="Create new recipe",
     *      @OA\RequestBody(
     *          required=true,
     *          @OA\JsonContent(
     *              ref="#/components/schemas/RecipeRequest"
     *          )
     *      ),
     *      @OA\Response(
     *          response="201",
     *          description="Success",
     *          @OA\JsonContent(
     *              ref="#/components/schemas/Recipe"
     *          )
     *      ),
     *      @OA\Response(
     *          response="406",
     *          description="Success",
     *          @OA\JsonContent(
     *              ref="#/components/schemas/ValidationErrors"
     *          )
     *      )
     * )
     * @param RecipeStoreRequest $request
     * @return RecipeResource
     */
    public function store(RecipeStoreRequest $request)
    {
        $request->validated();

        $recipe = new Recipe();

        $recipe->user_id = auth()->id();
        $recipe->name = $request->get('name');
        $recipe->description = $request->get('description', '');
        $recipe->portions = $request->get('portions', 1);
        $recipe->preparing_time = $request->get('preparing_time', 1);
        $recipe->estimated_price = $request->get('estimated_price', 1);
        $recipe->difficulty = $request->get('difficulty', 1);
        $recipe->category_id = $request->get('category_id', null);
        $recipe->share_type = $request->get('share_type', 0);
        $recipe->save();

        if ($request->get('share_type', 0) == Recipe::SHARE_TYPE_MAILS)
        {
            foreach ($request->get('shared_emails', []) as $sharedEmailRequest)
            {
                $sharedEmail = new RecipeShare();
                $sharedEmail->fill([
                    'object_id' => $recipe->id,
                    'email' => $sharedEmailRequest['value'],
                    'type' => Recipe::SHARE_TYPE_MAILS
                ]);
                $sharedEmail->save();
            }
        }
        elseif ($request->get('share_type', 0) == Recipe::SHARE_TYPE_ALL)
        {
            $sharedEmail = new RecipeShare();
            $sharedEmail->fill([
                'object_id' => $recipe->id,
                'email' => '',
                'type' => Recipe::SHARE_TYPE_ALL
            ]);
            $sharedEmail->save();
        }

        return new RecipeResource($recipe);
    }

    /**
     * @OA\Put(
     *      path="/api/recipes/{id}",
     *      security={{"BearerAuth":{}}},
     *      tags={"Recipes"},
     *      summary="Create new recipe",
     *      @OA\Parameter(
     *          parameter="id",
     *          name="id",
     *          in="path",
     *          required=true,
     *          @OA\Schema(
     *              type="integer"
     *          )
     *      ),
     *      @OA\RequestBody(
     *          required=true,
     *          @OA\JsonContent(
     *              ref="#/components/schemas/RecipeRequest"
     *          )
     *      ),
     *      @OA\Response(
     *          response="201",
     *          description="Success",
     *          @OA\JsonContent(
     *              ref="#/components/schemas/Recipe"
     *          )
     *      ),
     *      @OA\Response(
     *          response="406",
     *          description="Success",
     *          @OA\JsonContent(
     *              ref="#/components/schemas/ValidationErrors"
     *          )
     *      )
     * )
     * @param RecipeStoreRequest $request
     * @param $id
     * @return RecipeResource|\Illuminate\Http\JsonResponse
     */
    public function update(RecipeStoreRequest $request, $id)
    {
        $recipe = Recipe::query()->where('user_id', auth()->id())->find($id);

        if (!$recipe)
            return ApiResponses::objectNotFound(__('Przepis o podanym identyfikatorze nie istnieje'));

        $request->validated();

        $recipe->name = $request->get('name');
        $recipe->description = $request->get('description', '');
        $recipe->portions = $request->get('portions', 1);
        $recipe->preparing_time = $request->get('preparing_time', 1);
        $recipe->estimated_price = $request->get('estimated_price', 1);
        $recipe->difficulty = $request->get('difficulty', 1);
        $recipe->category_id = $request->get('category_id', null);
        $recipe->share_type = $request->get('share_type', 0);
        $recipe->save();

        RecipeShare::query()->where('object_id', $recipe->id)->delete();

        if ($request->get('share_type', 0) == Recipe::SHARE_TYPE_MAILS)
        {
            foreach ($request->get('shared_emails', []) as $sharedEmailRequest)
            {
                $sharedEmail = new RecipeShare();
                $sharedEmail->fill([
                    'object_id' => $recipe->id,
                    'email' => $sharedEmailRequest['value'],
                    'type' => Recipe::SHARE_TYPE_MAILS
                ]);
                $sharedEmail->save();
            }
        }
        elseif ($request->get('share_type', 0) == Recipe::SHARE_TYPE_ALL)
        {
            $sharedEmail = new RecipeShare();
            $sharedEmail->fill([
                'object_id' => $recipe->id,
                'email' => '',
                'type' => Recipe::SHARE_TYPE_ALL
            ]);
            $sharedEmail->save();
        }

        return new RecipeResource($recipe);
    }

    /**
     * @OA\Delete(
     *      path="/api/recipes/{id}",
     *      security={{"BearerAuth":{}}},
     *      tags={"Recipes"},
     *      summary="Delete recipe",
     *      @OA\Parameter(
     *          parameter="id",
     *          name="id",
     *          in="path",
     *          required=true,
     *          @OA\Schema(
     *              type="integer"
     *          )
     *      ),
     *      @OA\Response(
     *          response="200",
     *          description="Success"
     *      ),
     *      @OA\Response(
     *          response="404",
     *          description="Not found",
     *          @OA\JsonContent(
     *              ref="#/components/schemas/NotFoundError"
     *          )
     *      )
     * )
     * @param $id
     * @return \Illuminate\Http\JsonResponse
     * @throws \Exception
     */
    public function delete($id)
    {
        $recipe = Recipe::query()->where('user_id', auth()->id())->where('id', $id)->first();

        if (!$recipe)
            return ApiResponses::objectNotFound(__('Przepis o podanym identyfikatorze nie istnieje'));

        RecipeIngredient::query()->where('recipe_id', $recipe->id)->delete();
        RecipeStep::query()->where('recipe_id', $recipe->id)->delete();
        RecipePhoto::query()->where('recipe_id', $recipe->id)->delete();

        $recipe->delete();

        return ApiResponses::success();
    }

    /**
     * @OA\Patch(
     *      path="/api/recipes/{id}/toggleFavorite",
     *      security={{"BearerAuth":{}}},
     *      tags={"Recipes"},
     *      summary="Toggle recipe as favorites",
     *      @OA\Parameter(
     *          parameter="id",
     *          name="id",
     *          in="path",
     *          required=true,
     *          @OA\Schema(
     *              type="integer"
     *          )
     *      ),
     *      @OA\Response(
     *          response="200",
     *          description="Success"
     *      ),
     *      @OA\Response(
     *          response="404",
     *          description="Not found",
     *          @OA\JsonContent(
     *              ref="#/components/schemas/NotFoundError"
     *          )
     *      )
     * )
     * @param $id
     * @return \Illuminate\Http\JsonResponse
     * @throws \Exception
     */
    public function toggleFavorite($id)
    {
        $recipe = Recipe::findQuery()->where('user_id', auth()->id())->where('id', $id)->first();

        if (!$recipe)
            return ApiResponses::objectNotFound(__('Przepis o podanym identyfikatorze nie istnieje'));

        if (UserFavoriteRecipe::query()->where('user_id', auth()->id())->where('recipe_id', $id)->exists())
            UserFavoriteRecipe::query()->where('user_id', auth()->id())->where('recipe_id', $id)->delete();
        else
        {
            $userFavorite = new UserFavoriteRecipe();
            $userFavorite->user_id = auth()->id();
            $userFavorite->recipe_id = $id;
            $userFavorite->save();
        }

        return ApiResponses::success();
    }

    /**
     * @OA\Get(
     *      path="/api/recipes/favorite",
     *      security={{"BearerAuth":{}}},
     *      tags={"Recipes"},
     *      summary="Get recipes list",
     *      @OA\Parameter(
     *          parameter="page",
     *          name="page",
     *          in="query",
     *          @OA\Schema(
     *              type="integer",
     *              default=1
     *          )
     *      ),
     *      @OA\Parameter(
     *          parameter="perPage",
     *          name="perPage",
     *          in="query",
     *          @OA\Schema(
     *              type="integer",
     *              default=10
     *          )
     *      ),
     *      @OA\Parameter(
     *          parameter="orderBy",
     *          name="orderBy",
     *          in="query",
     *          @OA\Schema(
     *              type="string",
     *              default="date"
     *          )
     *      ),
     *      @OA\Parameter(
     *          parameter="orderDirection",
     *          name="orderDirection",
     *          in="query",
     *          @OA\Schema(
     *              type="string",
     *              default="desc"
     *          )
     *      ),
     *      @OA\Response(
     *          response="200",
     *          description="Success",
     *          @OA\JsonContent(
     *              @OA\Property(
     *                  property="data",
     *                  type="array",
     *                  @OA\Items(
     *                      ref="#/components/schemas/Recipe"
     *                  )
     *              ),
     *              @OA\Property(
     *                  property="links",
     *                  type="object",
     *                      ref="#/components/schemas/PagerLinks"
     *              ),
     *              @OA\Property(
     *                  property="meta",
     *                  type="object",
     *                      ref="#/components/schemas/PagerMeta"
     *              )
     *          )
     *      )
     * )
     */
    public function favorite()
    {
        $perPage = request()->get('perPage', 10);
        $page = request()->get('page', 1);

        $orderBy = 'created_at';
        $orderDirection = 'desc';

        return RecipeResource::collection(Recipe::findQuery()
            ->where('user_id', auth()->id())
            ->whereIn('id', UserFavoriteRecipe::query()->where('user_id', auth()->id())->select('recipe_id'))
            ->orderBy($orderBy, $orderDirection)
            ->paginate($perPage, ['*'], 'page', $page));
    }

    /**
     * @OA\Patch(
     *      path="/api/recipes/{id}/rate",
     *      security={{"BearerAuth":{}}},
     *      tags={"Recipes"},
     *      summary="Toggle recipe rate",
     *      @OA\Parameter(
     *          parameter="id",
     *          name="id",
     *          in="path",
     *          required=true,
     *          @OA\Schema(
     *              type="integer"
     *          )
     *      ),
     *      @OA\RequestBody(
     *          required=true,
     *          @OA\JsonContent(
     *              @OA\Property(
     *                  property="value",
     *                  type="integer"
     *              ),
     *          )
     *      ),
     *      @OA\Response(
     *          response="200",
     *          description="Success"
     *      ),
     *      @OA\Response(
     *          response="404",
     *          description="Not found",
     *          @OA\JsonContent(
     *              ref="#/components/schemas/NotFoundError"
     *          )
     *      )
     * )
     * @param $id
     * @return \Illuminate\Http\JsonResponse
     * @throws \Exception
     */
    public function rate($id)
    {
        $recipe = Recipe::findQuery()->where('user_id', auth()->id())->where('id', $id)->first();

        if (!$recipe)
            return ApiResponses::objectNotFound(__('Przepis o podanym identyfikatorze nie istnieje'));

        if (RecipeRate::query()->where('user_id', auth()->id())->where('recipe_id', $id)->exists())
            return ApiResponses::forbidden(__('Ten przepis został już przez Ciebie oceniony'), 'warn');

        $rate = new RecipeRate();
        $rate->user_id = auth()->id();
        $rate->recipe_id = $id;
        $rate->value = request()->input('value', 1);
        $rate->save();

        return ApiResponses::success();
    }

    /**
     * @OA\Get(
     *      path="/api/recipes/{id}/rate",
     *      security={{"BearerAuth":{}}},
     *      tags={"Recipes"},
     *      summary="Get recipe rating by id",
     *      @OA\Parameter(
     *          parameter="id",
     *          name="id",
     *          in="path",
     *          required=true,
     *          @OA\Schema(
     *              type="integer"
     *          )
     *      ),
     *      @OA\Response(
     *          response="200",
     *          description="Success",
     *          @OA\JsonContent(
     *              ref="#/components/schemas/RecipeRate"
     *          )
     *      ),
     *      @OA\Response(
     *          response="404",
     *          description="Ingredient not found",
     *          @OA\JsonContent(
     *              ref="#/components/schemas/NotFoundError"
     *          )
     *      )
     * )
     *
     * @param $id
     * @return RecipeRateResource|\Illuminate\Http\JsonResponse
     */
    public function showRate($id)
    {
        $recipe = Recipe::findQuery()->where('user_id', auth()->id())->find($id);

        if (!$recipe)
            return ApiResponses::objectNotFound(__('Przepis o podanym identyfikatorze nie istnieje'));

        return new RecipeRateResource($recipe);
    }
}
