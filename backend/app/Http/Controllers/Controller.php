<?php

namespace App\Http\Controllers;

use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Routing\Controller as BaseController;
use Tymon\JWTAuth\JWTAuth;

/**
 * @OA\Info(
 *      version="1.0.0",
 *      title="Documentaction for Przepisomat API",
 *      description="Documentaction for Przepisomat API",
 *      @OA\Contact(
 *          email="admin@admin.com"
 *      )
 * )
 *
 * @OA\SecurityScheme(
 *     securityScheme="BearerAuth",
 *     type="http",
 *     scheme="bearer"
 * )
 */
class Controller extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;

    protected $auth;

    public function __construct(JWTAuth $auth)
    {
        $this->auth = $auth;
    }

    /**
     * @OA\Schema(
     *     schema="PagerLinks",
     *     @OA\Property(
     *         property="first",
     *         type="string",
     *         example="http://www.api.com/something?page=1"
     *     ),
     *     @OA\Property(
     *         property="last",
     *         type="string",
     *         example="http://www.api.com/something?page=3"
     *     ),
     *     @OA\Property(
     *         property="prev",
     *         type="string",
     *         example="http://www.api.com/something?page=1"
     *     ),
     *     @OA\Property(
     *          property="next",
     *          type="string",
     *          example="http://www.api.com/something?page=2"
     *     )
     * )
     */

    /**
     * @OA\Schema(
     *     schema="PagerMeta",
     *     @OA\Property(
     *         property="current_page",
     *         type="number",
     *         example="1"
     *     ),
     *     @OA\Property(
     *         property="from",
     *         type="number",
     *         example="1"
     *     ),
     *     @OA\Property(
     *         property="last_page",
     *         type="number",
     *         example="3"
     *     ),
     *     @OA\Property(
     *         property="path",
     *         type="string",
     *         example="http://www.api.com/something"
     *     ),
     *     @OA\Property(
     *         property="per_page",
     *         type="number",
     *         example="10"
     *     ),
     *     @OA\Property(
     *         property="to",
     *         type="number",
     *         example="10"
     *     ),
     *     @OA\Property(
     *         property="total",
     *         type="number",
     *         example="30"
     *     )
     * )
     */
}
