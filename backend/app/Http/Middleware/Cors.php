<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Support\Facades\Log;

class Cors
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if(isset($_SERVER['HTTP_USER_AGENT']) && preg_match('/postman/', strtolower($_SERVER['HTTP_USER_AGENT'])) && env('APP_ENV', 'local') != 'local')
        {
            return false;
        }

        $allowedOrigins = [
            'http://localhost:4200',
            'http://localhost:3000'
        ];

        $response = $next($request);

        if(isset($_SERVER['HTTP_ORIGIN']))
        {
            $origin = $_SERVER['HTTP_ORIGIN'];
        }
        else
        {
            $origin = 'http://localhost:3000';
        }

        if(!in_array($origin, $allowedOrigins))
        {
            $origin = 'http://localhost:3000';
        }

        $response->header('Access-Control-Allow-Origin', $origin);
        $response->header('Access-Control-Allow-Methods', 'POST, GET, OPTIONS, PUT, DELETE, PATCH');
        $response->header('Access-Control-Allow-Headers', 'Content-Type, Accept, Authorization, X-Requested-With');
        $response->header('Access-Control-Allow-Credentials', 'true');

        $response->header('Access-Control-Expose-Headers', 'access-token');

        return $response;
    }
}
