<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddTableRecipes extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('recipes', function (Blueprint $table) {
            $table->id();
            $table->timestamps();
            $table->string('name');
            $table->text('description')->nullable();
            $table->bigInteger('user_id');
            $table->float('portions')->default(1);
            $table->float('estimated_price', 12, 2)->nullable();
            $table->integer('preparing_time')->nullable();
            $table->integer('difficulty')->default(1);
            $table->integer('category_id')->nullable();
            $table->integer('rating_count')->default(0);
            $table->integer('rating_value')->default(0);
            $table->integer('share_type')->default(0);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('recipes');
    }
}
