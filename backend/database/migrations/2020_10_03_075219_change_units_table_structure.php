<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class ChangeUnitsTableStructure extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('units', function (Blueprint $table) {
            $table->dropColumn('abbreviation');
            $table->string('abbr_quantity_one', 64);
            $table->string('abbr_quantity_float', 64);
            $table->string('abbr_quantity_two_four', 64);
            $table->string('abbr_quantity_five_and_more', 64);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('units', function (Blueprint $table) {
            $table->json('abbreviation');
            $table->dropColumn('abbr_quantity_one', 'abbr_quantity_float', 'abbr_quantity_two_four', 'abbr_quantity_five_and_more');
        });
    }
}
