<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::group(['prefix' => '/auth', ['middleware' => ['throttle:20,5']]], function(){
    Route::post('/register', 'Auth\RegisterController@postRegister');
    Route::post('/login', 'Auth\LoginController@postLogin');
});

Route::group(["middleware" => ['jwt', 'jwt.auth']], function (){
    Route::get('/me', 'UserController@getMe');
    Route::get('/auth/logout', 'Auth\LogoutController@getLogout');

    Route::group(['prefix' => '/users'], function (){
        Route::put('/', 'UserController@update');
        Route::put('/password', 'UserController@updatePassword');
        Route::put('/settings', 'UserController@updateSettings');
        Route::delete('/', 'UserController@delete');
    });

    Route::group(['prefix' => '/recipes'], function (){
        Route::get('/', 'RecipeController@index');
        Route::get('/favorite', 'RecipeController@favorite');
        Route::get('/{id}', 'RecipeController@show');
        Route::post('/', 'RecipeController@store');
        Route::put('/{id}', 'RecipeController@update');
        Route::delete('/{id}', 'RecipeController@delete');
        Route::patch('/{id}/toggleFavorite', 'RecipeController@toggleFavorite');
        Route::patch('/{id}/rate', 'RecipeController@rate');
        Route::get('/{id}/rate', 'RecipeController@showRate');

        Route::group(['prefix' => '/{recipeId}/ingredient'], function (){
            Route::get('/', 'RecipeIngredientController@index');
            Route::get('/{id}', 'RecipeIngredientController@show');
            Route::post('/', 'RecipeIngredientController@store');
            Route::put('/{id}', 'RecipeIngredientController@update');
            Route::delete('/{id}', 'RecipeIngredientController@delete');
       });

        Route::group(['prefix' => '/{recipeId}/step'], function (){
            Route::get('/', 'RecipeStepController@index');
            Route::get('/{id}', 'RecipeStepController@show');
            Route::post('/', 'RecipeStepController@store');
            Route::put('/{id}', 'RecipeStepController@update');
            Route::delete('/{id}', 'RecipeStepController@delete');
       });

        Route::group(['prefix' => '/{recipeId}/photo'], function (){
            Route::get('/', 'RecipePhotoController@index');
            Route::get('/{id}', 'RecipePhotoController@show');
            Route::post('/', 'RecipePhotoController@store');
            Route::put('/{id}', 'RecipePhotoController@update');
            Route::delete('/{id}', 'RecipePhotoController@delete');
       });
    });

    Route::group(['prefix' => '/units'], function (){
        Route::get('/', 'UnitsController@index');
        Route::get('/{id}', 'UnitsController@show');
        Route::post('/', 'UnitsController@store');
        Route::put('/{id}', 'UnitsController@update');
        Route::delete('/{id}', 'UnitsController@delete');
    });

    Route::group(['prefix' => '/ingredients'], function (){
        Route::get('/', 'IngredientsController@index');
        Route::get('/{id}', 'IngredientsController@show');
        Route::post('/', 'IngredientsController@store');
        Route::put('/{id}', 'IngredientsController@update');
        Route::delete('/{id}', 'IngredientsController@delete');
   });

    Route::group(['prefix' => '/categories'], function (){
        Route::get('/', 'CategoriesController@index');
        Route::get('/tree', 'CategoriesController@tree');
        Route::get('/{id}', 'CategoriesController@show');
        Route::post('/', 'CategoriesController@store');
        Route::put('/{id}', 'CategoriesController@update');
        Route::delete('/{id}', 'CategoriesController@delete');
   });

    Route::group(['prefix' => '/photos'], function (){
        Route::get('/', 'PhotosController@index');
        Route::get('/{id}', 'PhotosController@show');
        Route::post('/', 'PhotosController@store');
        Route::put('/{id}', 'PhotosController@update');
        Route::delete('/{id}', 'PhotosController@delete');
   });

    Route::group(['prefix' => '/cart'], function (){
        Route::get('/', 'CartController@index');
        Route::post('/', 'CartController@store');
        Route::delete('/{id}', 'CartController@delete');
        Route::delete('/{recipe_id}/{ingredient_id}', 'CartController@deleteOne');
    });
});

Route::fallback(function(){
    return response()->json(
    [
        "error" => "Page not found",
        "type" => "error"
    ]
    );
});
